/**
 * \file bezierinterpolator.h
 * \brief Plik nagłówkowy klasy \a bezierinterpolator.
 *
 * Klasa \a bezierinterpolator przechowuje informację takie jak
 * jakość iterpolacji, wagę środkowych punktów krzywej Beziera.
 * Oraz przede wszystkim ma zaimplementowane takie funkcję jak wyznaczanie punktów interpolacyjnych krzywej Beziera
 * zarówno z offsetem jak i bez, czy wyznaczenie punktów pomocniczych algorytmem Boehma
 *
 * \see bezierinterpolator.cpp
 */

#ifndef BorderPoint_H
#define BorderPoint_H

#include <cmath>
#include <Point.h>
class ControlPoint
{
public:
    double x;
    double y;
    ControlPoint();
    ControlPoint(double a, double b, bool c) ;
    void SetValue(double X, double Y);
    bool vector;
};

class BorderPoint
{
public:
    BorderPoint();
    //~BorderPoint();
    ControlPoint p;
    ControlPoint vPoint;
    ControlPoint ppPoint;
    bool reverseVector;

    double theta;
    double thetaStopnie;
    double V;

    void UpdateValues();
    void SetValue(double X1, double Y1, double X2, double Y2);
    void MoveToPosition(double X, double Y);
    void ApproximateVector(double alfa);
    void MoveAlongVector(double M);
    void MoveAlongVector(double X, double Y);

    void RadToDeg();
    void DegToRad();
private:

    double px,py;
};

#endif // BorderPoint_H
