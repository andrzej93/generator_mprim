/**
 * \file bezierinterpolator.h
 * \brief Plik nagłówkowy klasy \a bezierinterpolator.
 *
 * Klasa \a bezierinterpolator przechowuje informację takie jak
 * jakość iterpolacji, wagę środkowych punktów krzywej Beziera.
 * Oraz przede wszystkim ma zaimplementowane takie funkcję jak wyznaczanie punktów interpolacyjnych krzywej Beziera
 * zarówno z offsetem jak i bez, czy wyznaczenie punktów pomocniczych algorytmem Boehma
 *
 * \see bezierinterpolator.cpp
 */
#ifndef GENERATOR_TrajectoryOW_H
#define GENERATOR_TrajectoryOW_H

#include <fstream>
#include <iomanip>
#include "Trajectory.h"
#include "bezierinterpolator.h"
#include "Nurbs.h"

struct Trajectories_angle
{
    std::vector<Trajectory> trajektories;
    int angleID;
    int number_of_primitives;
};

class generator_motion_primitives
{
public:
    generator_motion_primitives();

    std::vector<Trajectories_angle> trajectories_angle;
    //std::vector<Trajectory> trajektorie;
    Trajectory Path;
    Nurbs nurbs;

     std::ofstream plik;
     std::string path_file;

    /**
     * @brief bezierInterpolator - Obiekt odpowiedzialny za przeliczenia punktów pomocniczych
     * oraz punktów krzywej (interpolowanej)
     */
    BezierInterpolator bezierInterpolator;

    /// Ograniczenia ruchu
    double Vmax;
    double Omegamax;

    /// Dane do pliku
    double resolution_m;
    int number_of_angles;
    int total_number_of_primitives;

    /// Rozdzielczosc
    double resolution;

    ///
    bool control_curvature_on_the_edges;
    bool control_deivative_edges;
    bool NurbsActive;
    bool Nurbs_control_deivative_edges;

    bool approximate_angle;
    double MultiplierPpp;

    double maxcurve;

    void interpolateCurve();

    void SaveTrajectory();
    void TurnLeft();
    void TurnRight();

    void Autocomplete();
    void SaveDataToFile();


    void ChangeNumberOfAngles();


private:
    void Przenies(std::vector<Trajectory>::iterator przenoszone, double x, double y, bool swap, int angle, int angle_act);
    void Przenies2(std::vector<Trajectory>::iterator przenoszone, double x, double y, bool swap, int angle, int angle_act);
};

#endif // GENERATOR_TrajectoryOW_H
