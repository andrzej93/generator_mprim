/**
 * \file bezierinterpolator.h
 * \brief Plik nagłówkowy klasy \a bezierinterpolator.
 *
 * Klasa \a bezierinterpolator przechowuje informację takie jak
 * jakość iterpolacji, wagę środkowych punktów krzywej Beziera.
 * Oraz przede wszystkim ma zaimplementowane takie funkcję jak wyznaczanie punktów interpolacyjnych krzywej Beziera
 * zarówno z offsetem jak i bez, czy wyznaczenie punktów pomocniczych algorytmem Boehma
 *
 * \see bezierinterpolator.cpp
 */
#ifndef PUNKT_H
#define PUNKT_H


struct Point
{
    double u;
    double x;
    double y;
    double theta;
    double curvature;
    struct vector_curvature
    {
        double x;
        double y;
    };
    vector_curvature V_curvature;
};

#endif // PUNKT_H
