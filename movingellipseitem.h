/**
 * \file movingellipseitem.h
 * \brief Plik nagłówkowy klasy \a movingellipseitem.
 *
 * Klasa \a movingellipseitem odpowiedzialna jest za umożliwienie poruszania
 * wierzchołków kontrolnych na grafice w interfejsie programu
 * Posiada funkcję typu MouseEvent reagująca na wszystkie zmiany pozycji wierzchołków
 *
 * \see movingellipseitem.cpp
 */

#ifndef MOVINGELLIPSEITEM_H
#define MOVINGELLIPSEITEM_H

#include "mainwindow.h"
#include <QGraphicsEllipseItem>
#include <QHash>

/**
 * @brief MovingEllipseItem - this class represents control point on QGraphicsView on \var mainWindow.
 */
class MovingEllipseItem : public QGraphicsEllipseItem {
public:
    MovingEllipseItem(qreal x, qreal y, qreal width, qreal height,
                    MainWindow *mainWindow, QGraphicsItem *parent = 0);
  
private:
    MainWindow *mainWindow;

    /**
     * @brief mouseMoveEvent - odpowiedzialna jest za umożliwienie poruszania
     * wierzchołków kontrolnych na grafice QGraphicsView w interfejsie programu, aktualizację pozycji
     * punktów kontrolnych w in \var mainWindow->controlPoints.
     * @param event - poruczenie dowolnego wierzchołka wieloboku kontrolnego (ale nie tylko, tylko to funkcja uwzględnia)
     */
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
};

#endif // MOVINGELLIPSEITEM_H
