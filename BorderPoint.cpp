/**
 * \file BorderPoint.cpp
 * \brief Plik implementacji klasy \a BorderPoint.
 *
 * Klasa \a BorderPoint przechowuje informację takie jak
 * \see BorderPoint.h
 */
#include "BorderPoint.h"
#include <iostream>

BorderPoint::BorderPoint()
{
    p.x = 0;
    p.y = 0;
    p.vector = 0;
    vPoint.x = 1;
    vPoint.y = 1;
    vPoint.vector = 1;
    ppPoint.x = 0;
    ppPoint.y = 5;
    ppPoint.vector = 0;
}

void BorderPoint::UpdateValues()
{
    py = vPoint.y - p.y;
    px = vPoint.x - p.x;

    V = std::sqrt(py*py + px*px);

    if(reverseVector)
    {
        theta = std::atan2(-py,-px);
    }
    else
    {
        theta = std::atan2(py,px);
    }
    RadToDeg();
}

void ControlPoint::SetValue(double X, double Y)
{
    x = X;
    y = Y;
}

void BorderPoint::SetValue(double X1, double Y1, double X2, double Y2)
{
    if(reverseVector)
    p.SetValue(X1,Y1);
    vPoint.SetValue(X2,Y2);
    UpdateValues();
}
void BorderPoint::MoveToPosition(double X, double Y)
{
    ppPoint.SetValue(ppPoint.x+(X-p.x),ppPoint.y+(Y-p.y));
    p.SetValue(X,Y);
    vPoint.SetValue(X+px, Y+py);
}

ControlPoint::ControlPoint(double a, double b, bool c)
{
    x = a;
    y = b;
    vector = c;
}

ControlPoint::ControlPoint()
{
    //x = 1;
    //y = 1;
    //vector = 1;
}

void BorderPoint::DegToRad()
{
    theta = (thetaStopnie/180)*M_PI;
}
void BorderPoint::RadToDeg()
{
    thetaStopnie = theta*180/M_PI;
}
void BorderPoint::ApproximateVector(double alfa)
{
    UpdateValues();
    double x = round( thetaStopnie / alfa);
    thetaStopnie = alfa*x;
    DegToRad();
    if(reverseVector)
    {
        vPoint.SetValue(-V*cos(theta)+p.x,-V*sin(theta)+p.y);
    }
    else
    {
        vPoint.SetValue(V*cos(theta)+p.x,V*sin(theta)+p.y);
    }
    UpdateValues();
}

void BorderPoint::MoveAlongVector(double M)
{
    V = M;
    vPoint.SetValue(V*cos(theta)+p.x,V*sin(theta)+p.y);
    UpdateValues();
}
void BorderPoint::MoveAlongVector(double X, double Y)
{
    py = Y - p.y;
    px = X - p.x;

    V = std::sqrt(py*py + px*px);
    vPoint.SetValue(V*cos(theta)+p.x,V*sin(theta)+p.y);
    UpdateValues();
}
