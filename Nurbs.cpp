/**
 * \file Nurbs.cpp
 * \brief Plik implementacji klasy \a Nurbs.
 *
 * Klasa \a Nurbs przechowuje informację takie jak
 * \see Nurbs.h
 */
#include "Nurbs.h"

Nurbs::Nurbs()
{
    ID = 0;
    additionalactioncostmult = 1;
    number_of_interpolation_points = 100;
    number_of_generat_mprim_points = 10;
    copied = 0;
    visibility = 0;
    firstPoint.reverseVector = 0;
    lastPoint.reverseVector = 1;

    thirdPoint.x = 100;
    thirdPoint.y = 100;
    fourthPoint.x = 150;
    fourthPoint.y = 100;
}

void Nurbs::calculate_Length()
{
    length = 0;
    for (std::vector<Point>::iterator pointIt = interpolation_points.begin(),
         pointEnd = interpolation_points.end(); pointIt != pointEnd; pointIt++)
    {
        if (pointIt != interpolation_points.end() - 1)
        {
            double tmp = std::sqrt( ((pointIt+1)->x - pointIt->x)*((pointIt+1)->x - pointIt->x) + ((pointIt+1)->y - pointIt->y)*((pointIt+1)->y - pointIt->y));
            length = length + tmp;
        }
    }
}
void Nurbs::copy(Trajectory trajectory)
{
    ID = trajectory.ID;
    number_of_interpolation_points = trajectory.number_of_interpolation_points;
    number_of_generat_mprim_points = trajectory.number_of_generat_mprim_points;
    additionalactioncostmult = trajectory.additionalactioncostmult;

    visibility = trajectory.visibility;
    copied = trajectory.copied;

    firstPoint = trajectory.firstpoint;
    lastPoint = trajectory.secondpoint;
}
void Nurbs::UpdatePoints()
{
    thirdPoint.x = 2*((firstPoint.ppPoint.x-firstPoint.p.x)+(firstPoint.vPoint.x-firstPoint.p.x))+firstPoint.vPoint.x;
    thirdPoint.y = 2*((firstPoint.ppPoint.y-firstPoint.p.y)+(firstPoint.vPoint.y-firstPoint.p.y))+firstPoint.vPoint.y;
    fourthPoint.x = 2*((lastPoint.ppPoint.x-lastPoint.p.x)+(lastPoint.vPoint.x-lastPoint.p.x))+lastPoint.vPoint.x;
    fourthPoint.y = 2*((lastPoint.ppPoint.y-lastPoint.p.y)+(lastPoint.vPoint.y-lastPoint.p.y))+lastPoint.vPoint.y;
}
