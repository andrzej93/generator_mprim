/**
 * \file movingellipseitem.cpp
 * \brief Plik implementacyjny klasy \a movingellipseitem.
 *
 * Klasa \a movingellipseitem odpowiedzialna jest za umożliwienie poruszania
 * wierzchołków kontrolnych na grafice w interfejsie programu
 * Posiada funkcję typu MouseEvent reagująca na wszystkie zmiany pozycji wierzchołków
 *
 * \see movingellipseitem.h
 */

#include "movingellipseitem.h"
#include <QGraphicsSceneMouseEvent>
#include <iostream>

MovingEllipseItem::MovingEllipseItem(qreal x, qreal y, qreal width,
                                     qreal height, MainWindow *mainWindow,
                                     QGraphicsItem *parent) :
    QGraphicsEllipseItem(x, y, width, height, parent), mainWindow(mainWindow) {}


void MovingEllipseItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event) {

    QGraphicsEllipseItem::mouseMoveEvent(event);
    const QPointF &pos = event->scenePos();
    ControlPoint *point = mainWindow->itemToPoint[this];
    mainWindow->update = 0;

    this->setRect(QRectF(1,1,1,1));
    double angle_resolution = 360.0/mainWindow->generator->number_of_angles;
    if(&mainWindow->generator->Path.firstpoint.vPoint == point)
    {
        mainWindow->generator->Path.firstpoint.vPoint.SetValue(pos.x()-mainWindow->centerpoint.x(), -pos.y()+mainWindow->centerpoint.y());
        mainWindow->generator->Path.firstpoint.ApproximateVector(angle_resolution);
        //mainWindow->generator->Path.firstpoint.PrzesunWzdluzWektora(pos.x()-mainWindow->centerpoint.x(),pos.y()-mainWindow->centerpoint.y());
    }
    else if(&mainWindow->generator->Path.secondpoint.vPoint == point)
    {
        mainWindow->generator->Path.secondpoint.vPoint.SetValue(pos.x()-mainWindow->centerpoint.x(), -pos.y()+mainWindow->centerpoint.y());
        mainWindow->generator->Path.secondpoint.ApproximateVector(angle_resolution);
    }
    else if(&mainWindow->generator->Path.firstpoint.p == point)
    {
        mainWindow->generator->Path.firstpoint.MoveToPosition(mainWindow->ApproximatePosition(pos.x())-mainWindow->centerpoint.x(),mainWindow->ApproximatePosition(-pos.y()+mainWindow->centerpoint.y()));
    }
    else if(&mainWindow->generator->Path.secondpoint.p == point)
    {
        mainWindow->generator->Path.secondpoint.MoveToPosition(mainWindow->ApproximatePosition(pos.x())-mainWindow->centerpoint.x(),mainWindow->ApproximatePosition(-pos.y())+mainWindow->centerpoint.y());
    }
    else if(&mainWindow->generator->Path.firstpoint.ppPoint == point)
    {
        mainWindow->generator->Path.firstpoint.ppPoint.SetValue((pos.x())-mainWindow->centerpoint.x(),(-pos.y()+mainWindow->centerpoint.y()));
    }
    else if(&mainWindow->generator->Path.secondpoint.ppPoint == point)
    {
        mainWindow->generator->Path.secondpoint.ppPoint.SetValue((pos.x())-mainWindow->centerpoint.x(),(-pos.y())+mainWindow->centerpoint.y());
    }
    else if(&mainWindow->generator->nurbs.firstPoint.vPoint == point)
    {
        mainWindow->generator->nurbs.firstPoint.vPoint.SetValue(pos.x()-mainWindow->centerpoint.x(), -pos.y()+mainWindow->centerpoint.y());
        mainWindow->generator->nurbs.firstPoint.ApproximateVector(angle_resolution);
        //mainWindow->generator->nurbs.firstPoint.PrzesunWzdluzWektora(pos.x()-mainWindow->centerpoint.x(),pos.y()-mainWindow->centerpoint.y());
    }
    else if(&mainWindow->generator->nurbs.lastPoint.vPoint == point)
    {
        mainWindow->generator->nurbs.lastPoint.vPoint.SetValue(pos.x()-mainWindow->centerpoint.x(), -pos.y()+mainWindow->centerpoint.y());
        mainWindow->generator->nurbs.lastPoint.ApproximateVector(angle_resolution);
    }
    else if(&mainWindow->generator->nurbs.firstPoint.p == point)
    {
        mainWindow->generator->nurbs.firstPoint.MoveToPosition(mainWindow->ApproximatePosition(pos.x())-mainWindow->centerpoint.x(),mainWindow->ApproximatePosition(-pos.y()+mainWindow->centerpoint.y()));
    }
    else if(&mainWindow->generator->nurbs.lastPoint.p == point)
    {
        mainWindow->generator->nurbs.lastPoint.MoveToPosition(mainWindow->ApproximatePosition(pos.x())-mainWindow->centerpoint.x(),mainWindow->ApproximatePosition(-pos.y())+mainWindow->centerpoint.y());
    }
    else if(&mainWindow->generator->nurbs.firstPoint.ppPoint == point)
    {
        mainWindow->generator->nurbs.firstPoint.ppPoint.SetValue((pos.x())-mainWindow->centerpoint.x(),(-pos.y()+mainWindow->centerpoint.y()));
    }
    else if(&mainWindow->generator->nurbs.lastPoint.ppPoint == point)
    {
        mainWindow->generator->nurbs.lastPoint.ppPoint.SetValue((pos.x())-mainWindow->centerpoint.x(),(-pos.y())+mainWindow->centerpoint.y());
    }
    else if(&mainWindow->generator->nurbs.thirdPoint == point)
    {
        mainWindow->generator->nurbs.thirdPoint.SetValue((pos.x())-mainWindow->centerpoint.x(),(-pos.y()+mainWindow->centerpoint.y()));
    }
    else if(&mainWindow->generator->nurbs.fourthPoint == point)
    {
        mainWindow->generator->nurbs.fourthPoint.SetValue((pos.x())-mainWindow->centerpoint.x(),(-pos.y())+mainWindow->centerpoint.y());
    }

    const QList<QGraphicsItem*> &items = mainWindow->scene->items();
    for (QList<QGraphicsItem*>::const_iterator itemIt = items.begin();
        itemIt != items.end(); ++itemIt)
    {
        if (*itemIt != this)
        {
            mainWindow->scene->removeItem(*itemIt);
            delete *itemIt;
        }
    }
    mainWindow->updateView();

}
void MovingEllipseItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event) {

    QGraphicsEllipseItem::mouseReleaseEvent(event);
    delete this;
    mainWindow->updateView();
}
