/**
 * \file Trajectory.cpp
 * \brief Plik implementacji klasy \a Trajectory.
 *
 * Klasa \a Trajectory przechowuje informację takie jak
 * \see Trajectory.h
 */
#include "Trajectory.h"
#include <iostream>

Trajectory::Trajectory()
{
    ID = 0;
    additionalactioncostmult = 1;
    number_of_interpolation_points = 100;
    number_of_generat_mprim_points = 10;
    copied = 0;
    visibility = 0;
    firstpoint.reverseVector = 0;
    secondpoint.reverseVector = 1;
}

void Trajectory::calculate_Length()
{
    length = 0;
    for (std::vector<Point>::iterator pointIt = interpolation_points.begin(),
         pointEnd = interpolation_points.end(); pointIt != pointEnd; pointIt++)
    {
        if (pointIt != interpolation_points.end() - 1)
        {
            double tmp = std::sqrt( ((pointIt+1)->x - pointIt->x)*((pointIt+1)->x - pointIt->x) + ((pointIt+1)->y - pointIt->y)*((pointIt+1)->y - pointIt->y));
            length = length + tmp;
        }
    }
}
