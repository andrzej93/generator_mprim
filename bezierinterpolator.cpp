/**
 * \file bezierinterpolator.cpp
 * \brief Plik implementacji klasy \a bezierinterpolator.
 *
 * Klasa \a bezierinterpolator przechowuje informację takie jak
 * \see bezierinterpolator.h
 */

#include "bezierinterpolator.h"

BezierInterpolator::BezierInterpolator() : Quality(0.01) {}

void BezierInterpolator::Bezier(Trajectory &Trajectory, double res, double M, bool approximate_angle )
{

    //Wczytanie potrzebnych danych
    double x1 = Trajectory.firstpoint.p.x;
    double y1 = Trajectory.firstpoint.p.y;
    double x4 = Trajectory.secondpoint.p.x;
    double y4 = Trajectory.secondpoint.p.y;

    double x1pp = Trajectory.firstpoint.ppPoint.x;
    double y1pp = Trajectory.firstpoint.ppPoint.y;
    double x4pp = Trajectory.secondpoint.ppPoint.x;
    double y4pp = Trajectory.secondpoint.ppPoint.y;

    double x2 = (((-2)*M*(x1pp-x1) - M*(x4pp-x4) - 6*x1 + 6*x4)/18) + x1;
    double y2 = (((-2)*M*(y1pp-y1) - M*(y4pp-y4) - 6*y1 + 6*y4)/18) + y1;
    double x3 = -((2*M*(x4pp-x4) + M*(x1pp-x1) - 6*x1 + 6*x4)/18) + x4;
    double y3 = -((2*M*(y4pp-y4) + M*(y1pp-y1) - 6*y1 + 6*y4)/18) + y4;

    Trajectory.firstpoint.vPoint.SetValue(x2,y2);
    Trajectory.secondpoint.vPoint.SetValue(x3,y3);

    if(approximate_angle)
    {
        Trajectory.firstpoint.ApproximateVector(22.5);
        Trajectory.secondpoint.ApproximateVector(22.5);
    }
    Bezier(Trajectory, res);
}

void BezierInterpolator::Bezier(Nurbs &nurbs,ControlPoint p1,ControlPoint p2,ControlPoint p3,ControlPoint p4, double res)
{

    //Wczytanie potrzebnych danych
    double x1 = p1.x;
    double y1 = p1.y;
    double x2 = p2.x;
    double y2 = p2.y;
    double x3 = p3.x;
    double y3 = p3.y;
    double x4 = p4.x;
    double y4 = p4.y;

    //Inicjalizacja potrzebnych zmiennych
    double w1 = 1.0;
    double w2 = 1.0;
    double w3 = 1.0;
    double w4 = 1.0;

    double krok = 0.01;
    double skal = 5000;

    bool warunek_na_krzywizne = 0;
    while (!warunek_na_krzywizne)
    {
        warunek_na_krzywizne = 1;
        poprawny_Trajectory = 1;

        double MaxCurvature = LimitCurvature;
        double uCurvature;

        for(int i = 0; i<nurbs.number_of_interpolation_points; i++)
        {
            double u = i*(1.0/(nurbs.number_of_interpolation_points-1));
            Point point;
            point.u = u;
            double u2 = u * u;          // u^2
            double u3 = u2 * u;         // u^3
            double u1 = 1 - u;          // (1-u)
            double u12 = u1 * u1;       // (1-u)^2
            double u13 = u12 * u1;      // (1-u)^3

            double Lpx = (w1*u13*x1 + 3*w2*u*u12*x2 + 3*w3*u2*u1*x3 + w4*u3*x4);                    // Qx(u)
            double Mpx = (w1*u13    + 3*w2*u*u12    + 3*w3*u2*u1    + w4*u3   );                    // w(u)
            double Lpy = (w1*u13*y1 + 3*w2*u*u12*y2 + 3*w3*u2*u1*y3 + w4*u3*y4);                    // Qy(u)
            double Mpy = (w1*u13    + 3*w2*u*u12    + 3*w3*u2*u1    + w4*u3   );                    // w(u)

            double px = Lpx / Mpx;                                                                  // Px(u)
            double py = Lpy / Mpy;                                                                  // Py(u)

            point.x = px;
            point.y = py;

            // Obliczenie 1 pochodnej
            double Lp11x = (-3)*u12*w1*x1 + 3*u12*x2*w2 - 6*u*u1*x2*w2 - 3*u2*x3*w3 + 6*u*u1*x3*w3 + 3*u2*x4*w4;   // Qx'(u)
            double Lp12x = (-3)*u12*w1    + 3*u12*w2    - 6*u*u1*w2    - 3*u2*w3    + 6*u*u1*w3    + 3*u2*w4;      // w'
            double p1x = ( Lp11x - px*Lp12x ) / Mpx;                                    // Px'(u) = (Qx'(u)-Px(u)*w'(u))/w(u)

            double Lp11y = (-3)*u12*w1*y1 + 3*u12*y2*w2 - 6*u*u1*y2*w2 - 3*u2*y3*w3 + 6*u*u1*y3*w3 + 3*u2*y4*w4;   // Qy'(u)
            double Lp12y = Lp12x;                                                                                  // w'
            double p1y = ( Lp11y - py*Lp12y ) / Mpy;                                    // Py'(u) = (Qy'(u)-Py(u)*w'(u))/w(u)

            // Obliczenie 2 pochodnej
            double Lp21x = (-6)*(u1*(-1)*w1*x1 + (2-3*u)*w2*x2 + 3*u*w3*x3 - w3*x3 - u*w4*x4);                   // Qx''(u)
            double Lp22x = (-6)*(u1*(-1)*w1    + (2-3*u)*w2    + 3*u*w3    - w3    - u*w4);                      // w''
            double p2x = ( Lp21x - 2*p1x*Lp12x - px*Lp22x ) / Mpx ;                     // Px'' = (Qx''-2Px'*w'-Px*w'')/w

            double Lp21y = (-6)*(u1*y1*(-1)*w1 + (2-3*u)*y2*w2 + 3*u*y3*w3 - y3*w3 - u*y4*w4);                   // Qx''(u)
            double Lp22y = Lp22x;                                                                                // w''
            double p2y = ( Lp21y - 2*p1y*Lp12y - py*Lp22y ) / Mpy ;                     // Px'' = (Qx''-2Px'*w'-Px*w'')/w

            // curvature
            double pom = std::sqrt( p1x*p1x + p1y*p1y );
            double K  = ( p1x*p2y - p2x*p1y ) / (pom*pom*pom);

            // Wektor krzywizny
            double wky = (-p1x/pom) * K * skal;
            double wkx = (p1y/pom) * K * skal;

            point.V_curvature.x = px+wkx;
            point.V_curvature.y = py+wky;

            K = K/res;
            point.curvature = K;

            point.theta = atan2(p1y,p1x);
/*
            if(Trajectory.firstpoint.reverseVector)
            {
                point.theta = atan2(-p1y,-p1x);
            }
            else
            {
                point.theta = atan2(p1y,p1x);
            }*/

            if(point.theta>2*M_PI)
            {
                point.theta = point.theta-(2*M_PI);
            }
            nurbs.interpolation_points.push_back(point);
        }
    }
}

void BezierInterpolator::Bezier(Trajectory &Trajectory, double res)
{

    //Wczytanie potrzebnych danych
    double x1 = Trajectory.firstpoint.p.x;
    double y1 = Trajectory.firstpoint.p.y;
    double x2 = Trajectory.firstpoint.vPoint.x;
    double y2 = Trajectory.firstpoint.vPoint.y;
    double x3 = Trajectory.secondpoint.vPoint.x;
    double y3 = Trajectory.secondpoint.vPoint.y;
    double x4 = Trajectory.secondpoint.p.x;
    double y4 = Trajectory.secondpoint.p.y;

    //Inicjalizacja potrzebnych zmiennych
    double w1 = 1.0;
    double w2 = 1.0;
    double w3 = 1.0;
    double w4 = 1.0;

    double krok = 0.01;
    double skal = 5000;

    bool warunek_na_krzywizne = 0;
    while (!warunek_na_krzywizne)
    {
        warunek_na_krzywizne = 1;
        poprawny_Trajectory = 1;

        double MaxCurvature = LimitCurvature;
        double uCurvature;

        Trajectory.interpolation_points.clear();

        for(int i = 0; i<Trajectory.number_of_interpolation_points; i++)
        {
            double u = i*(1.0/(Trajectory.number_of_interpolation_points-1));
            Point point;
            point.u = u;
            double u2 = u * u;          // u^2
            double u3 = u2 * u;         // u^3
            double u1 = 1 - u;          // (1-u)
            double u12 = u1 * u1;       // (1-u)^2
            double u13 = u12 * u1;      // (1-u)^3

            double Lpx = (w1*u13*x1 + 3*w2*u*u12*x2 + 3*w3*u2*u1*x3 + w4*u3*x4);                    // Qx(u)
            double Mpx = (w1*u13    + 3*w2*u*u12    + 3*w3*u2*u1    + w4*u3   );                    // w(u)
            double Lpy = (w1*u13*y1 + 3*w2*u*u12*y2 + 3*w3*u2*u1*y3 + w4*u3*y4);                    // Qy(u)
            double Mpy = (w1*u13    + 3*w2*u*u12    + 3*w3*u2*u1    + w4*u3   );                    // w(u)

            double px = Lpx / Mpx;                                                                  // Px(u)
            double py = Lpy / Mpy;                                                                  // Py(u)

            point.x = px;
            point.y = py;

            /*
             * Miejsce na dalsze obliczenie związane z punktami jak:
             * - Wyznaczenie kąta theta, wektora krzywizny, wartości krzywizny
             * - Sprawdzenie warunku na związanego z ograniczeniem krzywizyn
             */
            // Obliczenie 1 pochodnej
            double Lp11x = (-3)*u12*w1*x1 + 3*u12*x2*w2 - 6*u*u1*x2*w2 - 3*u2*x3*w3 + 6*u*u1*x3*w3 + 3*u2*x4*w4;   // Qx'(u)
            double Lp12x = (-3)*u12*w1    + 3*u12*w2    - 6*u*u1*w2    - 3*u2*w3    + 6*u*u1*w3    + 3*u2*w4;      // w'
            double p1x = ( Lp11x - px*Lp12x ) / Mpx;                                    // Px'(u) = (Qx'(u)-Px(u)*w'(u))/w(u)

            double Lp11y = (-3)*u12*w1*y1 + 3*u12*y2*w2 - 6*u*u1*y2*w2 - 3*u2*y3*w3 + 6*u*u1*y3*w3 + 3*u2*y4*w4;   // Qy'(u)
            double Lp12y = Lp12x;                                                                                  // w'
            double p1y = ( Lp11y - py*Lp12y ) / Mpy;                                    // Py'(u) = (Qy'(u)-Py(u)*w'(u))/w(u)

            // Obliczenie 2 pochodnej
            double Lp21x = (-6)*(u1*(-1)*w1*x1 + (2-3*u)*w2*x2 + 3*u*w3*x3 - w3*x3 - u*w4*x4);                   // Qx''(u)
            double Lp22x = (-6)*(u1*(-1)*w1    + (2-3*u)*w2    + 3*u*w3    - w3    - u*w4);                      // w''
            double p2x = ( Lp21x - 2*p1x*Lp12x - px*Lp22x ) / Mpx ;                     // Px'' = (Qx''-2Px'*w'-Px*w'')/w

            double Lp21y = (-6)*(u1*y1*(-1)*w1 + (2-3*u)*y2*w2 + 3*u*y3*w3 - y3*w3 - u*y4*w4);                   // Qx''(u)
            double Lp22y = Lp22x;                                                                                // w''
            double p2y = ( Lp21y - 2*p1y*Lp12y - py*Lp22y ) / Mpy ;                     // Px'' = (Qx''-2Px'*w'-Px*w'')/w

            // Krzywizna
            double pom = std::sqrt( p1x*p1x + p1y*p1y );
            double K  = ( p1x*p2y - p2x*p1y ) / (pom*pom*pom);

            // Wektor krzywizny
            double wky = (-p1x/pom) * K * skal;
            double wkx = (p1y/pom) * K * skal;

            point.V_curvature.x = px+wkx;
            point.V_curvature.y = py+wky;

            K = K/res;
            point.curvature = K;

            if(Trajectory.firstpoint.reverseVector)
                point.theta = atan2(-p1y,-p1x);
            else
                point.theta = atan2(p1y,p1x);

            if(point.theta>2*M_PI)
            {
                point.theta = point.theta-(2*M_PI);
            }

            Trajectory.interpolation_points.push_back(point);

            if( (K > MaxCurvature) || (K < (-1)*MaxCurvature))
            {
                uCurvature = u;
                MaxCurvature = K;
                poprawny_Trajectory = 0;
            }
        }
        if(!poprawny_Trajectory)
            for (std::vector<Point>::iterator pointIt = Trajectory.interpolation_points.begin(),
                pointEnd = Trajectory.interpolation_points.end(); pointIt != pointEnd; pointIt++)
        {
            if(uCurvature == pointIt->u)
            {
                // Znalezienie najbliższego wierzchołka
                int l = 1;
                double minodl = odleglosc(pointIt->x, pointIt->y, x1, y1);
                if (minodl > odleglosc(pointIt->x, pointIt->y, x2, y2))
                {
                    l = 2;
                    minodl = odleglosc(pointIt->x, pointIt->y, x2, y2);
                }
                if (minodl > odleglosc(pointIt->x, pointIt->y, x3, y3))
                {
                    l = 3;
                    minodl = odleglosc(pointIt->x, pointIt->y, x3, y3);
                }
                if (minodl > odleglosc(pointIt->x, pointIt->y, x4, y4))
                {
                    l = 4;
                    minodl = odleglosc(pointIt->x, pointIt->y, x4, y4);
                }

                // Zmniejszenie wagi odpowiedniego wierzchołka
                if((w1 >= 0)&&(w2 >= 0)&&(w3 >= 0)&&(w4 >= 0))

                {
                    switch( l )
                        {
                        case 1:
                            if((w1 > krok))
                            {
                                w1 = w1 - krok;
                                warunek_na_krzywizne = 0;
                            }
                            break;
                        case 2:
                            if(w2 > krok)
                            {
                                w2 = w2 - krok;
                                warunek_na_krzywizne = 0;
                            }
                            break;
                        case 3:
                            if(w3 > krok)
                            {
                                w3 = w3 - krok;
                                warunek_na_krzywizne = 0;
                            }
                            break;
                        case 4:
                            if((w4 > krok))
                            {
                                w4 = w4 - krok;
                                warunek_na_krzywizne = 0;
                            }
                            break;
                        default:
                            break;
                        }
                }
            }
        }
    }

    Trajectory.mprim_points.clear();
    for(int i = 0; i<Trajectory.number_of_generat_mprim_points; i++)
    {
        double u = i*(1.0/(Trajectory.number_of_generat_mprim_points-1));
        Point point;
        point.u = u;
        double u2 = u * u;          // u^2
        double u3 = u2 * u;         // u^3
        double u1 = 1 - u;          // (1-u)
        double u12 = u1 * u1;       // (1-u)^2
        double u13 = u12 * u1;      // (1-u)^3

        double Lpx = (w1*u13*x1 + 3*w2*u*u12*x2 + 3*w3*u2*u1*x3 + w4*u3*x4);                    // Qx(u)
        double Mpx = (w1*u13    + 3*w2*u*u12    + 3*w3*u2*u1    + w4*u3   );                    // w(u)
        double Lpy = (w1*u13*y1 + 3*w2*u*u12*y2 + 3*w3*u2*u1*y3 + w4*u3*y4);                    // Qy(u)
        double Mpy = (w1*u13    + 3*w2*u*u12    + 3*w3*u2*u1    + w4*u3   );                    // w(u)

        double px = Lpx / Mpx;                                                                  // Px(u)
        double py = Lpy / Mpy;                                                                  // Py(u)

        point.x = px;
        point.y = py;

        // Obliczenie 1 pochodnej
        double Lp11x = (-3)*u12*w1*x1 + 3*u12*x2*w2 - 6*u*u1*x2*w2 - 3*u2*x3*w3 + 6*u*u1*x3*w3 + 3*u2*x4*w4;   // Qx'(u)
        double Lp12x = (-3)*u12*w1    + 3*u12*w2    - 6*u*u1*w2    - 3*u2*w3    + 6*u*u1*w3    + 3*u2*w4;      // w'
        double p1x = ( Lp11x - px*Lp12x ) / Mpx;                                    // Px'(u) = (Qx'(u)-Px(u)*w'(u))/w(u)

        double Lp11y = (-3)*u12*w1*y1 + 3*u12*y2*w2 - 6*u*u1*y2*w2 - 3*u2*y3*w3 + 6*u*u1*y3*w3 + 3*u2*y4*w4;   // Qy'(u)
        double Lp12y = Lp12x;                                                                                  // w'
        double p1y = ( Lp11y - py*Lp12y ) / Mpy;                                    // Py'(u) = (Qy'(u)-Py(u)*w'(u))/w(u)

        // Obliczenie 2 pochodnej
        double Lp21x = (-6)*(u1*(-1)*w1*x1 + (2-3*u)*w2*x2 + 3*u*w3*x3 - w3*x3 - u*w4*x4);                   // Qx''(u)
        double Lp22x = (-6)*(u1*(-1)*w1    + (2-3*u)*w2    + 3*u*w3    - w3    - u*w4);                      // w''
        double p2x = ( Lp21x - 2*p1x*Lp12x - px*Lp22x ) / Mpx ;                     // Px'' = (Qx''-2Px'*w'-Px*w'')/w

        double Lp21y = (-6)*(u1*y1*(-1)*w1 + (2-3*u)*y2*w2 + 3*u*y3*w3 - y3*w3 - u*y4*w4);                   // Qx''(u)
        double Lp22y = Lp22x;                                                                                // w''
        double p2y = ( Lp21y - 2*p1y*Lp12y - py*Lp22y ) / Mpy ;                     // Px'' = (Qx''-2Px'*w'-Px*w'')/w

        /// curvature
        double pom = std::sqrt( p1x*p1x + p1y*p1y );
        double K  = ( p1x*p2y - p2x*p1y ) / (pom*pom*pom);

        point.curvature = K;

        // Wektor krzywizny
        double wky = (-p1x/pom) * K * skal;
        double wkx = (p1y/pom) * K * skal;

        point.V_curvature.x = px+wkx;
        point.V_curvature.y = py+wky;

        if(Trajectory.firstpoint.reverseVector)
        {
            point.theta = atan2(-p1y,-p1x);
        }
        else
        {
            point.theta = atan2(p1y,p1x);
        }

        if(point.theta>2*M_PI)
        {
            point.theta = point.theta-(2*M_PI);
        }

        Trajectory.mprim_points.push_back(point);

        }

}


/// \brief Bezier - funkcja interpolująca Pointy krzywej Beziera 3 stopnia

///
/// \brief BezierInterpolator::BezierNormal
/// \param Trajectory
///
void BezierInterpolator::BezierNormal(Trajectory &Trajectory)
{
    double w1 = 1.0;
    double w2 = 1.0;
    double w3 = 1.0;
    double w4 = 1.0;

    double skal = 5000;

    Trajectory.interpolation_points.clear();

    double x1 = Trajectory.firstpoint.p.x;
    double y1 = Trajectory.firstpoint.p.y;
    double x2 = Trajectory.firstpoint.vPoint.x;
    double y2 = Trajectory.firstpoint.vPoint.y;
    double x3 = Trajectory.secondpoint.vPoint.x;
    double y3 = Trajectory.secondpoint.vPoint.y;
    double x4 = Trajectory.secondpoint.p.x;
    double y4 = Trajectory.secondpoint.p.y;

    for(double u=0; u<=1; u+=(1.0/(Trajectory.number_of_interpolation_points-1))
        )
    {

        Point point;
        point.u = u;
        double u2 = u * u;          // u^2
        double u3 = u2 * u;         // u^3
        double u1 = 1 - u;          // (1-u)
        double u12 = u1 * u1;       // (1-u)^2
        double u13 = u12 * u1;      // (1-u)^3

        double Lpx = (w1*u13*x1 + 3*w2*u*u12*x2 + 3*w3*u2*u1*x3 + w4*u3*x4);                    // Qx(u)
        double Mpx = (w1*u13    + 3*w2*u*u12    + 3*w3*u2*u1    + w4*u3   );                    // w(u)
        double Lpy = (w1*u13*y1 + 3*w2*u*u12*y2 + 3*w3*u2*u1*y3 + w4*u3*y4);                    // Qy(u)
        double Mpy = (w1*u13    + 3*w2*u*u12    + 3*w3*u2*u1    + w4*u3   );                    // w(u)

        double px = Lpx / Mpx;                                                                  // Px(u)
        double py = Lpy / Mpy;                                                                  // Py(u)

        point.x = px;
        point.y = py;

        // Obliczenie 1 pochodnej
        double Lp11x = (-3)*u12*w1*x1 + 3*u12*x2*w2 - 6*u*u1*x2*w2 - 3*u2*x3*w3 + 6*u*u1*x3*w3 + 3*u2*x4*w4;   // Qx'(u)
        double Lp12x = (-3)*u12*w1    + 3*u12*w2    - 6*u*u1*w2    - 3*u2*w3    + 6*u*u1*w3    + 3*u2*w4;      // w'
        double p1x = ( Lp11x - px*Lp12x ) / Mpx;                                    // Px'(u) = (Qx'(u)-Px(u)*w'(u))/w(u)

        double Lp11y = (-3)*u12*w1*y1 + 3*u12*y2*w2 - 6*u*u1*y2*w2 - 3*u2*y3*w3 + 6*u*u1*y3*w3 + 3*u2*y4*w4;   // Qy'(u)
        double Lp12y = Lp12x;                                                                                  // w'
        double p1y = ( Lp11y - py*Lp12y ) / Mpy;                                    // Py'(u) = (Qy'(u)-Py(u)*w'(u))/w(u)

        // Obliczenie 2 pochodnej
        double Lp21x = (-6)*(u1*(-1)*w1*x1 + (2-3*u)*w2*x2 + 3*u*w3*x3 - w3*x3 - u*w4*x4);                   // Qx''(u)
        double Lp22x = (-6)*(u1*(-1)*w1    + (2-3*u)*w2    + 3*u*w3    - w3    - u*w4);                      // w''
        double p2x = ( Lp21x - 2*p1x*Lp12x - px*Lp22x ) / Mpx ;                     // Px'' = (Qx''-2Px'*w'-Px*w'')/w

        double Lp21y = (-6)*(u1*y1*(-1)*w1 + (2-3*u)*y2*w2 + 3*u*y3*w3 - y3*w3 - u*y4*w4);                   // Qx''(u)
        double Lp22y = Lp22x;                                                                                // w''
        double p2y = ( Lp21y - 2*p1y*Lp12y - py*Lp22y ) / Mpy ;                     // Px'' = (Qx''-2Px'*w'-Px*w'')/w

        /// curvature
        double pom = std::sqrt( p1x*p1x + p1y*p1y );
        double K  = ( p1x*p2y - p2x*p1y ) / (pom*pom*pom);
        point.curvature = K;

        // Wektor krzywizny
        double wky = (-p1x/pom) * K * skal;
        double wkx = (p1y/pom) * K * skal;

        point.V_curvature.x = px+wkx;
        point.V_curvature.y = py+wky;

        point.theta = p1y/p1x;

        Trajectory.interpolation_points.push_back(point);

    }
}

/// \brief Bezier - funkcja interpolująca Pointy krzywej Beziera 3 stopnia
void BezierInterpolator::BezierNormal(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4,
                                      QPolygonF &interpolatedPoints, QVector<double> &curvature, QPolygonF &curvaturePoints)
{
    double w1 = 1.0;
    double w2 = 1.0;
    double w3 = 1.0;
    double w4 = 1.0;

    double skal = 5000;

    for(double u=0; u<=1; u+=(Quality))
    {
        double u2 = u * u;          // u^2
        double u3 = u2 * u;         // u^3
        double u1 = 1 - u;          // (1-u)
        double u12 = u1 * u1;       // (1-u)^2
        double u13 = u12 * u1;      // (1-u)^3

        double Lpx = (w1*u13*x1 + 3*w2*u*u12*x2 + 3*w3*u2*u1*x3 + w4*u3*x4);                    // Qx(u)
        double Mpx = (w1*u13    + 3*w2*u*u12    + 3*w3*u2*u1    + w4*u3   );                    // w(u)
        double Lpy = (w1*u13*y1 + 3*w2*u*u12*y2 + 3*w3*u2*u1*y3 + w4*u3*y4);                    // Qy(u)
        double Mpy = (w1*u13    + 3*w2*u*u12    + 3*w3*u2*u1    + w4*u3   );                    // w(u)

        double px = Lpx / Mpx;                                                                  // Px(u)
        double py = Lpy / Mpy;                                                                  // Py(u)

        interpolatedPoints.push_back(QPointF(px, py));

        // Obliczenie 1 pochodnej
        double Lp11x = (-3)*u12*w1*x1 + 3*u12*x2*w2 - 6*u*u1*x2*w2 - 3*u2*x3*w3 + 6*u*u1*x3*w3 + 3*u2*x4*w4;   // Qx'(u)
        double Lp12x = (-3)*u12*w1    + 3*u12*w2    - 6*u*u1*w2    - 3*u2*w3    + 6*u*u1*w3    + 3*u2*w4;      // w'
        double p1x = ( Lp11x - px*Lp12x ) / Mpx;                                    // Px'(u) = (Qx'(u)-Px(u)*w'(u))/w(u)

        double Lp11y = (-3)*u12*w1*y1 + 3*u12*y2*w2 - 6*u*u1*y2*w2 - 3*u2*y3*w3 + 6*u*u1*y3*w3 + 3*u2*y4*w4;   // Qy'(u)
        double Lp12y = Lp12x;                                                                                  // w'
        double p1y = ( Lp11y - py*Lp12y ) / Mpy;                                    // Py'(u) = (Qy'(u)-Py(u)*w'(u))/w(u)

        // Obliczenie 2 pochodnej
        double Lp21x = (-6)*(u1*(-1)*w1*x1 + (2-3*u)*w2*x2 + 3*u*w3*x3 - w3*x3 - u*w4*x4);                   // Qx''(u)
        double Lp22x = (-6)*(u1*(-1)*w1    + (2-3*u)*w2    + 3*u*w3    - w3    - u*w4);                      // w''
        double p2x = ( Lp21x - 2*p1x*Lp12x - px*Lp22x ) / Mpx ;                     // Px'' = (Qx''-2Px'*w'-Px*w'')/w

        double Lp21y = (-6)*(u1*y1*(-1)*w1 + (2-3*u)*y2*w2 + 3*u*y3*w3 - y3*w3 - u*y4*w4);                   // Qx''(u)
        double Lp22y = Lp22x;                                                                                // w''
        double p2y = ( Lp21y - 2*p1y*Lp12y - py*Lp22y ) / Mpy ;                     // Px'' = (Qx''-2Px'*w'-Px*w'')/w

        /// curvature
        double pom = std::sqrt( p1x*p1x + p1y*p1y );
        double K  = ( p1x*p2y - p2x*p1y ) / (pom*pom*pom);
        curvature.push_back(K);

        // Wektor krzywizny
        double wky = (-p1x/pom) * K * skal;
        double wkx = (p1y/pom) * K * skal;

        curvaturePoints.push_back(QPointF(px+wkx, py+wky));

    }
}

void BezierInterpolator::SetMaxCurvature(double value)
{
    LimitCurvature = value;
}

double BezierInterpolator::odleglosc(double x1, double y1, double x2, double y2)
{
    double odlegl = ((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));
    return sqrt(odlegl);
}
