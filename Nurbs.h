/**
 * \file bezierinterpolator.h
 * \brief Plik nagłówkowy klasy \a bezierinterpolator.
 *
 * Klasa \a bezierinterpolator przechowuje informację takie jak
 * jakość iterpolacji, wagę środkowych punktów krzywej Beziera.
 * Oraz przede wszystkim ma zaimplementowane takie funkcję jak wyznaczanie punktów interpolacyjnych krzywej Beziera
 * zarówno z offsetem jak i bez, czy wyznaczenie punktów pomocniczych algorytmem Boehma
 *
 * \see bezierinterpolator.cpp
 */
#ifndef NURBS_H
#define NURBS_H

#include <vector>
#include "Point.h"
#include "BorderPoint.h"
#include "Trajectory.h"

class Nurbs
{
public:
    Nurbs();

    int ID;
    int number_of_interpolation_points;
    int number_of_generat_mprim_points;
    int additionalactioncostmult;

    double length;

    bool visibility;
    bool copied;

    std::vector<Point> interpolation_points;
    std::vector<Point> mprim_points;

    BorderPoint firstPoint;
    ControlPoint thirdPoint;
    ControlPoint fourthPoint;
    BorderPoint lastPoint;

    void calculate_Length();
    void copy(Trajectory trajectory);
    void UpdatePoints();
};

#endif // NURBS_H
