/**
 * \file MainWindow.cpp
 * \brief Plik implementacji klasy \a bezierinterpolator.
 *
 * Klasa \a bezierinterpolator przechowuje informację takie jak
 * \see MainWindow.h
 */
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "movingellipseitem.h"
#include <cmath>
#include <string>
#include <sstream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    generator = new generator_motion_primitives();
    update = 1;

    scene = new QGraphicsScene();
    ui->graphicsView->setScene(scene);
    ui->graphicsView->setRenderHint(QPainter::Antialiasing, true);
    ui->graphicsView->setRenderHint(QPainter::HighQualityAntialiasing, true);

    ui->treeWidget->setColumnCount(3);
    ui->treeWidget->setHeaderLabels(QStringList() <<"Angle/ID"<<"ID/Position"<<"Show");

    ui->treeWidget->header()->resizeSection(0,85);
    ui->treeWidget->header()->resizeSection(1,110);
    ui->treeWidget->header()->resizeSection(2,1);

    generator->number_of_angles = ui->L_angleSpinBox->value();
    generator->Vmax = ui->VelocityMaxSpinBox->value();
    generator->Omegamax = ui->OmegaMaxSpinBox->value();
    generator->Path.number_of_generat_mprim_points = ui->Number_mprim_point_SpinBox->value();
    generator->Path.number_of_interpolation_points = ui->Number_interpolate_point_spointSpinBox->value();
    generator->control_curvature_on_the_edges = displaySettings.control_curvature_on_the_edges;
    generator->control_deivative_edges = displaySettings.control_deivative_edges;
    generator->Nurbs_control_deivative_edges = displaySettings.Nurbs_control_deivative_edges;
    generator->NurbsActive = displaySettings.NurbsActive;
    generator->MultiplierPpp = ui->MultiplierPppBox->value();
    generator->approximate_angle = ui->approximate_angleBox->checkState();

    setCenterPoint();

    showRandomSpline();
    updateView();

    ui->xP0ppBox->setValue((generator->Path.firstpoint.ppPoint.x-generator->Path.firstpoint.p.x)/generator->resolution);
    ui->yP0ppBox->setValue((generator->Path.firstpoint.ppPoint.y-generator->Path.firstpoint.p.y)/generator->resolution);
    ui->LP0ppBox->setValue((generator->Path.firstpoint.ppPoint.x-generator->Path.firstpoint.p.x)/generator->resolution);
    ui->xP1ppBox->setValue((generator->Path.secondpoint.ppPoint.x-generator->Path.secondpoint.p.x)/generator->resolution);
    ui->yP1ppBox->setValue((generator->Path.secondpoint.ppPoint.y-generator->Path.secondpoint.p.y)/generator->resolution);
    ui->LP1ppBox->setValue((generator->Path.secondpoint.ppPoint.x-generator->Path.secondpoint.p.x)/generator->resolution);
    ui->AP0pBox->setValue(generator->Path.firstpoint.thetaStopnie);
    ui->LP0pBox->setValue((generator->Path.firstpoint.V)/generator->resolution);
    ui->AP1pBox->setValue(generator->Path.secondpoint.thetaStopnie);
    ui->LP1pBox->setValue((generator->Path.secondpoint.V)/generator->resolution);

    updateTree();

    QObject::connect(ui->treeWidget, SIGNAL(itemChanged(QTreeWidgetItem*,int)), this, SLOT(itemChang(QTreeWidgetItem*,int)));
}

MainWindow::~MainWindow()
{
    delete ui;
    delete scene;
    clearPoints();
}

void MainWindow::makePlot()
{
    double dl;
    if(generator->NurbsActive)
        dl = 3*generator->Path.number_of_interpolation_points;
    else
        dl = generator->Path.number_of_interpolation_points;
    // generate some data:
    QVector<double> x(dl), y(dl); // initialize with entries 0..100
    QVector<double> max(dl), min(dl);

    for (int i=0; i < dl; i++)
    {
        x[i] = i*(1.0/(generator->Path.number_of_interpolation_points-1));
        if(generator->NurbsActive)
            y[i] = generator->nurbs.interpolation_points[i].curvature;
        else
            y[i] = generator->Path.interpolation_points[i].curvature;
        max[i] = generator->bezierInterpolator.LimitCurvature;
        min[i] = (-1)*generator->bezierInterpolator.LimitCurvature;
    }
    ui->CurvatureP0Box->setValue(y[0]);
    ui->CurvatureP1Box->setValue(y[dl-1]);

    // create graph and assign data to it:
    ui->customPlot->addGraph();
    ui->customPlot->graph(0)->setData(x, y);
    ui->customPlot->graph(0)->setPen(QPen(Qt::black));
    ui->customPlot->addGraph();
    ui->customPlot->graph(1)->setData(x,max);
    ui->customPlot->graph(1)->setPen(QPen(Qt::red));
    ui->customPlot->addGraph();
    ui->customPlot->graph(2)->setData(x,min);
    ui->customPlot->graph(2)->setPen(QPen(Qt::red));
    ui->customPlot->addGraph();

    // give the axes some labels:
    ui->customPlot->xAxis->setLabel("u");
    ui->customPlot->yAxis->setLabel("K");

    // set axes ranges, so we see all data:
    ui->customPlot->xAxis->rescale(true);
    ui->customPlot->yAxis->rescale(true);

    ui->customPlot->replot();
}

/// clearPoints - Usuwa wszystkie Pointy
void MainWindow::clearPoints()
{
    generator->Path.interpolation_points.clear();
}

/// showRandomSpline - Generuje losowe Pointy + uatkualnia widok (wraz z krzywą)
void MainWindow::setCenterPoint()
{
    int x_border = ui->graphicsView->width();
    int y_border = ui->graphicsView->height();
    centerpoint.setX(x_border/2);
    centerpoint.setY(y_border/2);
}

/// showRandomSpline - Generuje losowe Pointy + uatkualnia widok (wraz z krzywą)
void MainWindow::showRandomSpline()
{
    scene->clear();
    clearPoints();
    generator->Path.firstpoint.SetValue(0,0,25,0);
    generator->Path.secondpoint.SetValue(200,50,175,25);
    generator->Path.firstpoint.ppPoint.SetValue(100,500);
    generator->Path.secondpoint.ppPoint.SetValue(200,100);

    /*
    generator->nurbs.firstPoint.SetValue(0,0,25,0);
    generator->nurbs.lastPoint.SetValue(200,50,175,25);
    generator->nurbs.firstPoint.ppPoint.SetValue(100,500);
    generator->nurbs.lastPoint.ppPoint.SetValue(200,100);
    */

    updateView();
}


/// updateView - Uuaktualnia widok krzywej na grafice graphicsView.
void MainWindow::updateView()
{
    generator->interpolateCurve();

    generator->Path.calculate_Length();
    QString qstr = QString::fromStdString(to_string(generator->Path.length*generator->resolution_m/generator->resolution));
    ui->lengthline->setText(qstr);


    setCenterPoint();
    QPen pen;
    if (generator->bezierInterpolator.poprawny_Trajectory)
    {
        ui->SaveButton->setStyleSheet("background-color: Chartreuse"); //jasny zielony
        ui->SaveButton->setText("Save trajectory");
        ui->Button->setStyleSheet("background-color: Chartreuse"); //jasny zielony
        ui->Button->setText("Trajectory poprawny");
    }
    else
    {
        ui->SaveButton->setStyleSheet("background-color: red");
        ui->SaveButton->setText("Reduce curvature");
        ui->Button->setStyleSheet("background-color: red");
        ui->Button->setText("Correct trajectory, reduce curvature");
    }
    int x = ui->graphicsView->width();
    int y = ui->graphicsView->height();

    //pen.setStyle(Qt::DashLine);
    pen.setStyle(Qt::DashDotLine);
    pen.setColor("black");
    pen.setWidth(1);

    // Rysowanie linii poziomych.
    for (int i = 0; i < y/generator->resolution; i++)
    {
        scene->addLine(QLineF(QPointF(centerpoint.x() - x/2,centerpoint.y() - y/2 + i*generator->resolution), QPointF(centerpoint.x() + x/2, centerpoint.y() - y/2 + i*generator->resolution)),pen);
    }

    // Rysowanie linii pionowych.
    for (int i = 0; i < x/generator->resolution; i++)
    {
        scene->addLine(QLineF(QPointF(centerpoint.x() - x/2 + i*generator->resolution, centerpoint.y() - y/2), QPointF(centerpoint.x() - x/2 + i*generator->resolution, centerpoint.y() + y/2)), pen);
    }/**/

    pen.setStyle(Qt::SolidLine);
    pen.setColor("black");
    pen.setWidth(2);

    // Line układu współrzędnych
    scene->addLine(QLineF(QPointF(centerpoint.x() - x/2,centerpoint.y()), QPointF(centerpoint.x() + x/2, centerpoint.y())),pen);
    scene->addLine(QLineF(QPointF(centerpoint.x(), centerpoint.y() - y/2), QPointF(centerpoint.x(), centerpoint.y() + y/2)), pen);

    if(!generator->NurbsActive)
        drawBezier();
    else
        drawNurbs();
    drawExistingTrajectories();

    makePlot();
    UpdatePppValues();
    UpdateNurbsValues();
    update = 1;

}

void MainWindow::drawBezier()
{
    // Show interpolated curve.

    for (std::vector<Point>::iterator pointIt = generator->Path.interpolation_points.begin(),
         pointEnd = generator->Path.interpolation_points.end(); pointIt != pointEnd; pointIt++)
    {
        if (pointIt != generator->Path.interpolation_points.end() - 1)
            scene->addLine(QLineF(QPointF(pointIt->x+centerpoint.x(),-pointIt->y+centerpoint.y()),
                                  QPointF((pointIt + 1)->x+centerpoint.x(),-(pointIt + 1)->y+centerpoint.y())),
                           QPen("green"));
        if (displaySettings.showCurvature)
        {
            scene->addLine(QLineF(QPointF(pointIt->x+centerpoint.x(),-pointIt->y+centerpoint.y()),
                                  QPointF(pointIt->V_curvature.x+centerpoint.x(), -pointIt->V_curvature.y+centerpoint.y())),
                           QPen("red"));
        }
        if (displaySettings.showInterpolatedPoints)
            scene->addEllipse(pointIt->x+centerpoint.x(),-pointIt->y+centerpoint.y(), 2, 2, QPen("black"),
                                QBrush("black"));
    }

    const int controlPointSize = 10;
    MovingEllipseItem *pointItem1 = new MovingEllipseItem(
                                 generator->Path.firstpoint.p.x + centerpoint.x() - controlPointSize / 2,
                                 -generator->Path.firstpoint.p.y + centerpoint.y() - controlPointSize / 2,
                                 controlPointSize, controlPointSize, this);
    itemToPoint[pointItem1] = &generator->Path.firstpoint.p;
    pointItem1->setBrush(QBrush("red"));
    pointItem1->setFlag(QGraphicsItem::ItemIsMovable, true);
    scene->addItem(pointItem1);

    MovingEllipseItem *pointItem2 = new MovingEllipseItem(
                                 generator->Path.secondpoint.p.x + centerpoint.x() - controlPointSize / 2,
                                 -generator->Path.secondpoint.p.y + centerpoint.y() - controlPointSize / 2,
                                 controlPointSize, controlPointSize, this);
    itemToPoint[pointItem2] = &generator->Path.secondpoint.p;
    pointItem2->setBrush(QBrush("red"));
    pointItem2->setFlag(QGraphicsItem::ItemIsMovable, true);
    scene->addItem(pointItem2);

    MovingEllipseItem *pointItemvec1 = new MovingEllipseItem(
                                 generator->Path.firstpoint.vPoint.x + centerpoint.x() - controlPointSize / 2,
                                 -generator->Path.firstpoint.vPoint.y + centerpoint.y() - controlPointSize / 2,
                                 controlPointSize, controlPointSize, this);
    itemToPoint[pointItemvec1] = &generator->Path.firstpoint.vPoint;
    pointItemvec1->setBrush(QBrush("blue"));
    pointItemvec1->setFlag(QGraphicsItem::ItemIsMovable, true);
    scene->addItem(pointItemvec1);

    MovingEllipseItem *pointItemvec2 = new MovingEllipseItem(
                                 generator->Path.secondpoint.vPoint.x + centerpoint.x() - controlPointSize / 2,
                                 -generator->Path.secondpoint.vPoint.y + centerpoint.y() - controlPointSize / 2,
                                 controlPointSize, controlPointSize, this);
    itemToPoint[pointItemvec2] = &generator->Path.secondpoint.vPoint;
    pointItemvec2->setBrush(QBrush("blue"));
    pointItemvec2->setFlag(QGraphicsItem::ItemIsMovable, true);
    scene->addItem(pointItemvec2);

    // Show control points.
    scene->addLine(QLineF(QPointF(generator->Path.firstpoint.p.x+centerpoint.x(), -generator->Path.firstpoint.p.y+centerpoint.y()),
                          QPointF(generator->Path.firstpoint.vPoint.x+centerpoint.x(),-generator->Path.firstpoint.vPoint.y+centerpoint.y())), QPen("blue"));
    scene->addLine(QLineF(QPointF(generator->Path.firstpoint.vPoint.x+centerpoint.x(), -generator->Path.firstpoint.vPoint.y+centerpoint.y()),
                          QPointF(generator->Path.secondpoint.vPoint.x+centerpoint.x(),-generator->Path.secondpoint.vPoint.y+centerpoint.y())), QPen("blue"));
    scene->addLine(QLineF(QPointF(generator->Path.secondpoint.vPoint.x+centerpoint.x(), -generator->Path.secondpoint.vPoint.y+centerpoint.y()),
                          QPointF(generator->Path.secondpoint.p.x+centerpoint.x(),-generator->Path.secondpoint.p.y+centerpoint.y())), QPen("blue"));

    if(displaySettings.control_curvature_on_the_edges)
    {
        drawPointsPP();
    }
}

void MainWindow::drawNurbs()
{
    // Show interpolated curve.
    for (std::vector<Point>::iterator pointIt = generator->nurbs.interpolation_points.begin(),
         pointEnd = generator->nurbs.interpolation_points.end(); pointIt != pointEnd; pointIt++)
    {
        if (pointIt != generator->nurbs.interpolation_points.end() - 1)
            scene->addLine(QLineF(QPointF(pointIt->x+centerpoint.x(),-pointIt->y+centerpoint.y()),
                                  QPointF((pointIt + 1)->x+centerpoint.x(),-(pointIt + 1)->y+centerpoint.y())),
                           QPen("green"));
        if (displaySettings.showCurvature)
        {
            scene->addLine(QLineF(QPointF(pointIt->x+centerpoint.x(),-pointIt->y+centerpoint.y()),
                                  QPointF(pointIt->V_curvature.x+centerpoint.x(), -pointIt->V_curvature.y+centerpoint.y())),
                           QPen("red"));
        }
        if (displaySettings.showInterpolatedPoints)
            scene->addEllipse(pointIt->x+centerpoint.x(),-pointIt->y+centerpoint.y(), 2, 2, QPen("black"),
                                QBrush("black"));
    }

    const int controlPointSize = 10;
    MovingEllipseItem *pointItem1 = new MovingEllipseItem(
                                 generator->nurbs.firstPoint.p.x + centerpoint.x() - controlPointSize / 2,
                                 -generator->nurbs.firstPoint.p.y + centerpoint.y() - controlPointSize / 2,
                                 controlPointSize, controlPointSize, this);
    itemToPoint[pointItem1] = &generator->nurbs.firstPoint.p;
    pointItem1->setBrush(QBrush("red"));
    pointItem1->setFlag(QGraphicsItem::ItemIsMovable, true);
    scene->addItem(pointItem1);

    MovingEllipseItem *pointItem2 = new MovingEllipseItem(
                                 generator->nurbs.lastPoint.p.x + centerpoint.x() - controlPointSize / 2,
                                 -generator->nurbs.lastPoint.p.y + centerpoint.y() - controlPointSize / 2,
                                 controlPointSize, controlPointSize, this);
    itemToPoint[pointItem2] = &generator->nurbs.lastPoint.p;
    pointItem2->setBrush(QBrush("red"));
    pointItem2->setFlag(QGraphicsItem::ItemIsMovable, true);
    scene->addItem(pointItem2);

    MovingEllipseItem *pointItemvec1 = new MovingEllipseItem(
                                 generator->nurbs.firstPoint.vPoint.x + centerpoint.x() - controlPointSize / 2,
                                 -generator->nurbs.firstPoint.vPoint.y + centerpoint.y() - controlPointSize / 2,
                                 controlPointSize, controlPointSize, this);
    itemToPoint[pointItemvec1] = &generator->nurbs.firstPoint.vPoint;
    pointItemvec1->setBrush(QBrush("blue"));
    pointItemvec1->setFlag(QGraphicsItem::ItemIsMovable, true);
    scene->addItem(pointItemvec1);

    MovingEllipseItem *pointItemvec2 = new MovingEllipseItem(
                                 generator->nurbs.lastPoint.vPoint.x + centerpoint.x() - controlPointSize / 2,
                                 -generator->nurbs.lastPoint.vPoint.y + centerpoint.y() - controlPointSize / 2,
                                 controlPointSize, controlPointSize, this);
    itemToPoint[pointItemvec2] = &generator->nurbs.lastPoint.vPoint;
    pointItemvec2->setBrush(QBrush("blue"));
    pointItemvec2->setFlag(QGraphicsItem::ItemIsMovable, true);
    scene->addItem(pointItemvec2);

    // Show control points.
    scene->addLine(QLineF(QPointF(generator->nurbs.firstPoint.p.x+centerpoint.x(), -generator->nurbs.firstPoint.p.y+centerpoint.y()),
                          QPointF(generator->nurbs.firstPoint.vPoint.x+centerpoint.x(),-generator->nurbs.firstPoint.vPoint.y+centerpoint.y())), QPen("blue"));
    scene->addLine(QLineF(QPointF(generator->nurbs.firstPoint.vPoint.x+centerpoint.x(), -generator->nurbs.firstPoint.vPoint.y+centerpoint.y()),
                          QPointF(generator->nurbs.thirdPoint.x+centerpoint.x(),-generator->nurbs.thirdPoint.y+centerpoint.y())), QPen("blue"));
    scene->addLine(QLineF(QPointF(generator->nurbs.thirdPoint.x+centerpoint.x(), -generator->nurbs.thirdPoint.y+centerpoint.y()),
                          QPointF(generator->nurbs.fourthPoint.x+centerpoint.x(),-generator->nurbs.fourthPoint.y+centerpoint.y())), QPen("blue"));
    scene->addLine(QLineF(QPointF(generator->nurbs.fourthPoint.x+centerpoint.x(), -generator->nurbs.fourthPoint.y+centerpoint.y()),
                          QPointF(generator->nurbs.lastPoint.vPoint.x+centerpoint.x(),-generator->nurbs.lastPoint.vPoint.y+centerpoint.y())), QPen("blue"));
    scene->addLine(QLineF(QPointF(generator->nurbs.lastPoint.vPoint.x+centerpoint.x(), -generator->nurbs.lastPoint.vPoint.y+centerpoint.y()),
                          QPointF(generator->nurbs.lastPoint.p.x+centerpoint.x(),-generator->nurbs.lastPoint.p.y+centerpoint.y())), QPen("blue"));

    if(generator->Nurbs_control_deivative_edges)
    {
        MovingEllipseItem *pointItempp1 = new MovingEllipseItem(
                                     generator->nurbs.firstPoint.ppPoint.x + centerpoint.x() - controlPointSize / 2,
                                     -generator->nurbs.firstPoint.ppPoint.y + centerpoint.y() - controlPointSize / 2,
                                     controlPointSize, controlPointSize, this);
        itemToPoint[pointItempp1] = &generator->nurbs.firstPoint.ppPoint;
        pointItempp1->setBrush(QBrush("yellow"));
        pointItempp1->setFlag(QGraphicsItem::ItemIsMovable, true);
        scene->addItem(pointItempp1);

        MovingEllipseItem *pointItempp2 = new MovingEllipseItem(
                                     generator->nurbs.lastPoint.ppPoint.x + centerpoint.x() - controlPointSize / 2,
                                     -generator->nurbs.lastPoint.ppPoint.y + centerpoint.y() - controlPointSize / 2,
                                     controlPointSize, controlPointSize, this);
        itemToPoint[pointItempp2] = &generator->nurbs.lastPoint.ppPoint;
        pointItempp2->setBrush(QBrush("yellow"));
        pointItempp2->setFlag(QGraphicsItem::ItemIsMovable, true);
        scene->addItem(pointItempp2);

        scene->addLine(QLineF(QPointF(generator->nurbs.firstPoint.p.x+centerpoint.x(), -generator->nurbs.firstPoint.p.y+centerpoint.y()),
                              QPointF(generator->nurbs.firstPoint.ppPoint.x+centerpoint.x(),-generator->nurbs.firstPoint.ppPoint.y+centerpoint.y())), QPen("yellow"));
        scene->addLine(QLineF(QPointF(generator->nurbs.lastPoint.ppPoint.x+centerpoint.x(), -generator->nurbs.lastPoint.ppPoint.y+centerpoint.y()),
                              QPointF(generator->nurbs.lastPoint.p.x+centerpoint.x(),-generator->nurbs.lastPoint.p.y+centerpoint.y())), QPen("yellow"));
    }


    MovingEllipseItem *pointItem3 = new MovingEllipseItem(
                                 generator->nurbs.thirdPoint.x + centerpoint.x() - controlPointSize / 2,
                                 -generator->nurbs.thirdPoint.y + centerpoint.y() - controlPointSize / 2,
                                 controlPointSize, controlPointSize, this);
    itemToPoint[pointItem3] = &generator->nurbs.thirdPoint;
    pointItem3->setBrush(QBrush("green"));
    pointItem3->setFlag(QGraphicsItem::ItemIsMovable, true);
    scene->addItem(pointItem3);

    MovingEllipseItem *pointItem4 = new MovingEllipseItem(
                                 generator->nurbs.fourthPoint.x + centerpoint.x() - controlPointSize / 2,
                                 -generator->nurbs.fourthPoint.y + centerpoint.y() - controlPointSize / 2,
                                 controlPointSize, controlPointSize, this);
    itemToPoint[pointItem4] = &generator->nurbs.fourthPoint;
    pointItem4->setBrush(QBrush("green"));
    pointItem4->setFlag(QGraphicsItem::ItemIsMovable, true);
    scene->addItem(pointItem4);
}
void MainWindow::drawExistingTrajectories()
{
    QPen pen;
    pen.setStyle(Qt::SolidLine);
    pen.setColor("red");
    pen.setWidth(1);

    //Show all Curve
    for (int i = 0; i < generator->number_of_angles; i++)
    {
        for (std::vector<Trajectory>::iterator TrajectoryIt = generator->trajectories_angle[i].trajektories.begin(),
             TrajectoryEnd = generator->trajectories_angle[i].trajektories.end(); TrajectoryIt != TrajectoryEnd; TrajectoryIt++)
        {
            if(displaySettings.showAllCurve)
            {
                for (std::vector<Point>::iterator pointIt = TrajectoryIt->interpolation_points.begin(),
                     pointEnd = TrajectoryIt->interpolation_points.end()-1; pointIt != pointEnd; pointIt++)
                {
                    if (pointIt != generator->Path.interpolation_points.end() - 1)
                        scene->addLine(QLineF(QPointF(pointIt->x+centerpoint.x(),-pointIt->y+centerpoint.y()),
                                              QPointF((pointIt + 1)->x+centerpoint.x(),-(pointIt + 1)->y+centerpoint.y())),pen);
                }
            }
            if(displaySettings.showAngleCurve)
            {
                if(TrajectoryIt->firstpoint.theta == generator->Path.firstpoint.theta)
                {
                    for (std::vector<Point>::iterator pointIt = TrajectoryIt->interpolation_points.begin(),
                         pointEnd = TrajectoryIt->interpolation_points.end()-1; pointIt != pointEnd; pointIt++)
                    {
                        if (pointIt != generator->Path.interpolation_points.end() - 1)
                            scene->addLine(QLineF(QPointF(pointIt->x+centerpoint.x(),-pointIt->y+centerpoint.y()),
                                                  QPointF((pointIt + 1)->x+centerpoint.x(),-(pointIt + 1)->y+centerpoint.y())),pen);
                    }
                }
            }
            if(TrajectoryIt->visibility)
            {
                for (std::vector<Point>::iterator pointIt = TrajectoryIt->interpolation_points.begin(),
                     pointEnd = TrajectoryIt->interpolation_points.end()-1; pointIt != pointEnd; pointIt++)
                {
                    if (pointIt != generator->Path.interpolation_points.end() - 1)
                        scene->addLine(QLineF(QPointF(pointIt->x+centerpoint.x(),-pointIt->y+centerpoint.y()),
                                              QPointF((pointIt + 1)->x+centerpoint.x(),-(pointIt + 1)->y+centerpoint.y())),pen);
                }
            }
        }
    }
}
void MainWindow::updateTree()
{
    int i = 0;
    ui->treeWidget->clear();
    for (std::vector<Trajectories_angle>::iterator trajectories_angleIt = generator->trajectories_angle.begin(),
         trajectories_angleEnd = generator->trajectories_angle.end(); trajectories_angleIt != trajectories_angleEnd; trajectories_angleIt++)
    {
        QTreeWidgetItem *itm = new QTreeWidgetItem(ui->treeWidget);
        QString qstr0 = QString::fromStdString("Kat: " + to_string(trajectories_angleIt->angleID*360/generator->number_of_angles));
        QString qstr1 = QString::fromStdString(to_string(trajectories_angleIt->angleID));
        itm->setFlags(itm->flags() | Qt::ItemIsUserCheckable | Qt::ItemIsSelectable);

        itm->setText(0,qstr0);
        itm->setText(1,qstr1);

        ui->treeWidget->addTopLevelItem(itm);
        int j = 0;
        for (std::vector<Trajectory>::iterator TrajectoryIt = trajectories_angleIt->trajektories.begin(),
             TrajectoryEnd = trajectories_angleIt->trajektories.end(); TrajectoryIt != TrajectoryEnd; TrajectoryIt++)
        {
            QTreeWidgetItem *itmchild = new QTreeWidgetItem();
            itmchild->setFlags(itmchild->flags() | Qt::ItemIsUserCheckable | Qt::ItemIsSelectable);
            QString qstr = QString::fromStdString("[" + to_string(int(TrajectoryIt->firstpoint.p.x/generator->resolution)) + "," + to_string(int(TrajectoryIt->firstpoint.p.y/generator->resolution)) + "," + to_string(int(TrajectoryIt->firstpoint.thetaStopnie)) + "]"  +
                                                  "[" + to_string(int(TrajectoryIt->secondpoint.p.x/generator->resolution)) + "," + to_string(int(TrajectoryIt->secondpoint.p.y/generator->resolution)) + "," + to_string(int(TrajectoryIt->secondpoint.thetaStopnie)) + "]");

            //itmchild->setText(0,QString::fromStdString("ID: " + to_string(TrajectoryIt->ID)));
            itmchild->setText(0,QString::fromStdString(to_string(TrajectoryIt->ID)));
            itmchild->setText(1,qstr);
            if(generator->trajectories_angle[i].trajektories[j].visibility)
                itmchild->setCheckState(2,Qt::Checked);
            else
                itmchild->setCheckState(2,Qt::Unchecked);

            itm->addChild(itmchild);
            j++;
        }
        i++;
    }
}

/// moveCurve - Przesuwa Pointy kontrolne (wierzchołki)
void MainWindow::moveCurve() {
    scene->clear();
    updateView();
}

void MainWindow::on_checkBox_1_stateChanged(int arg1) {
    displaySettings.showControlLines = arg1;
    scene->clear();
    updateView();
}
void MainWindow::on_checkBox_2_stateChanged(int arg1) {
    displaySettings.showCurvature = arg1;
    scene->clear();
    updateView();
}
void MainWindow::on_show_interpolated_pointscheckBox_stateChanged(int arg1)
{
    displaySettings.showInterpolatedPoints = arg1;
    scene->clear();
    updateView();
}

double MainWindow::ApproximatePosition(double var)
{
    double x = round( var / generator->resolution);
    double a = generator->resolution*x;
    return a;
}

void MainWindow::on_SaveButton_clicked()
{
    // Do czasu poprawy drzewa
    QObject::disconnect(ui->treeWidget, SIGNAL(itemChanged(QTreeWidgetItem*,int)), this, SLOT(itemChang(QTreeWidgetItem*,int)));
    generator->SaveTrajectory();
    updateTree();
    ui->AutocompleteButton->setStyleSheet("background-color: Yellow");
    QObject::connect(ui->treeWidget, SIGNAL(itemChanged(QTreeWidgetItem*,int)), this, SLOT(itemChang(QTreeWidgetItem*,int)));
}
void MainWindow::on_LeftButton_clicked()
{
    QObject::disconnect(ui->treeWidget, SIGNAL(itemChanged(QTreeWidgetItem*,int)), this, SLOT(itemChang(QTreeWidgetItem*,int)));
    generator->TurnLeft();
    updateTree();
    ui->AutocompleteButton->setStyleSheet("background-color: Yellow");
    QObject::connect(ui->treeWidget, SIGNAL(itemChanged(QTreeWidgetItem*,int)), this, SLOT(itemChang(QTreeWidgetItem*,int)));
}
void MainWindow::on_RightButton_clicked()
{
    QObject::disconnect(ui->treeWidget, SIGNAL(itemChanged(QTreeWidgetItem*,int)), this, SLOT(itemChang(QTreeWidgetItem*,int)));
    generator->TurnRight();
    updateTree();
    ui->AutocompleteButton->setStyleSheet("background-color: Yellow");
    QObject::connect(ui->treeWidget, SIGNAL(itemChanged(QTreeWidgetItem*,int)), this, SLOT(itemChang(QTreeWidgetItem*,int)));
}
void MainWindow::on_SaveToFileButton_clicked()
{
    generator->path_file = ui->path_file_line->text().toStdString();
    //generator->path_file = "/home/andrzej/Projekt/generator_mprim/wyniki/pr.mprim";
    generator->SaveDataToFile();
}

void MainWindow::on_Number_interpolate_point_spointSpinBox_valueChanged(double arg1)
{
    generator->Path.number_of_interpolation_points = arg1;
    scene->clear();
    updateView();
}

void MainWindow::on_Number_mprim_point_SpinBox_valueChanged(double arg1)
{
    generator->Path.number_of_generat_mprim_points = arg1;
    scene->clear();
    updateView();
}

void MainWindow::on_ResolutionSpinBox_valueChanged(double arg1)
{
    generator->resolution_m = arg1;
    scene->clear();
    updateView();
}

void MainWindow::on_L_angleSpinBox_valueChanged(double arg1)
{
    generator->number_of_angles = arg1;
    generator->ChangeNumberOfAngles();
    scene->clear();
    updateView();
}

void MainWindow::on_CostSpinBox_valueChanged(double arg1)
{
    generator->Path.additionalactioncostmult = arg1;
}

void MainWindow::on_VelocityMaxSpinBox_valueChanged(double arg1)
{
    generator->Vmax = arg1;
    scene->clear();
    updateView();
}

void MainWindow::on_OmegaMaxSpinBox_valueChanged(double arg1)
{
    generator->Omegamax = arg1;
    scene->clear();
    updateView();
}

void MainWindow::on_AutocompleteButton_clicked()
{
    QObject::disconnect(ui->treeWidget, SIGNAL(itemChanged(QTreeWidgetItem*,int)), this, SLOT(itemChang(QTreeWidgetItem*,int)));
    generator->Autocomplete();
    ui->AutocompleteButton->setStyleSheet("background-color: Light green");
    updateTree();
    scene->clear();
    updateView();
    QObject::connect(ui->treeWidget, SIGNAL(itemChanged(QTreeWidgetItem*,int)), this, SLOT(itemChang(QTreeWidgetItem*,int)));
}

void MainWindow::on_All_curve_Box_stateChanged(int arg1)
{
    displaySettings.showAllCurve = arg1;
    scene->clear();
    updateView();
}

void MainWindow::on_Angle_curve_checkBox_stateChanged(int arg1)
{
    displaySettings.showAngleCurve = arg1;
    scene->clear();
    updateView();
}

void MainWindow::itemChang(QTreeWidgetItem *item, int column)
{
    int angle = item->parent()->text(1).toInt();
    if(column == 2)
    {
        if(item->checkState(2))
            generator->trajectories_angle[angle].trajektories[item->text(0).toInt()].visibility = true;
        else
            generator->trajectories_angle[angle].trajektories[item->text(0).toInt()].visibility = false;
    }
    else if(column == 3)
    {
        if(item->checkState(3))
            generator->trajectories_angle[angle].trajektories[item->text(0).toInt()].copied = true;
        else
            generator->trajectories_angle[angle].trajektories[item->text(0).toInt()].copied = false;
    }
    scene->clear();
    updateView();
}

void MainWindow::on_ForgetButton_clicked()
{
    for (std::vector<Trajectories_angle>::iterator trajectories_angleIt = generator->trajectories_angle.begin(),
         trajectories_angleEnd = generator->trajectories_angle.end(); trajectories_angleIt != trajectories_angleEnd; trajectories_angleIt++)
    {
        trajectories_angleIt->trajektories.clear();
    }
}

void MainWindow::on_BackMovecheckBox_stateChanged(int arg1)
{
    displaySettings.backmove = arg1;
    generator->Path.firstpoint.reverseVector = arg1;
    generator->Path.secondpoint.reverseVector = !arg1;

    generator->Path.firstpoint.vPoint.SetValue(-generator->Path.firstpoint.vPoint.x, -generator->Path.firstpoint.vPoint.y);
    generator->Path.secondpoint.p.SetValue(-generator->Path.secondpoint.p.x, -generator->Path.secondpoint.p.y);
    generator->Path.secondpoint.vPoint.SetValue(-generator->Path.secondpoint.vPoint.x, -generator->Path.secondpoint.vPoint.y);

    scene->clear();
    updateView();
}


template <typename T>
std::string MainWindow::to_string(T value)
{
  //create an output string stream
  std::ostringstream os ;

  //throw the value into the string stream
  os << value ;

  //convert the string stream into a string and return
  return os.str() ;
}

void MainWindow::on_control_curvature_edges_Box_stateChanged(int arg1)
{
    displaySettings.control_curvature_on_the_edges = arg1;
    generator->control_curvature_on_the_edges = arg1;

    ui->xP0ppBox->setEnabled(arg1);
    ui->yP0ppBox->setEnabled(arg1);
    ui->LP0ppBox->setEnabled(arg1);
    ui->xP1ppBox->setEnabled(arg1);
    ui->yP1ppBox->setEnabled(arg1);
    ui->LP1ppBox->setEnabled(arg1);

    scene->clear();
    updateView();
}
void MainWindow::on_control_deivative_edges_Box_stateChanged(int arg1)
{
    displaySettings.control_deivative_edges = arg1;
    generator->control_deivative_edges = arg1;

    ui->AP0pBox->setEnabled(arg1);
    ui->LP0pBox->setEnabled(arg1);
    ui->AP1pBox->setEnabled(arg1);
    ui->LP1pBox->setEnabled(arg1);

    scene->clear();
    updateView();
}

void MainWindow::drawPointsPP()
{
    //UpdatePppValues();
    const int controlPointSize = 10;

    MovingEllipseItem *pointItempp1 = new MovingEllipseItem(
                                 generator->Path.firstpoint.ppPoint.x + centerpoint.x() - controlPointSize / 2,
                                 -generator->Path.firstpoint.ppPoint.y + centerpoint.y() - controlPointSize / 2,
                                 controlPointSize, controlPointSize, this);
    itemToPoint[pointItempp1] = &generator->Path.firstpoint.ppPoint;
    pointItempp1->setBrush(QBrush("yellow"));
    pointItempp1->setFlag(QGraphicsItem::ItemIsMovable, true);
    scene->addItem(pointItempp1);

    MovingEllipseItem *pointItempp2 = new MovingEllipseItem(
                                 generator->Path.secondpoint.ppPoint.x + centerpoint.x() - controlPointSize / 2,
                                 -generator->Path.secondpoint.ppPoint.y + centerpoint.y() - controlPointSize / 2,
                                 controlPointSize, controlPointSize, this);
    itemToPoint[pointItempp2] = &generator->Path.secondpoint.ppPoint;
    pointItempp2->setBrush(QBrush("yellow"));
    pointItempp2->setFlag(QGraphicsItem::ItemIsMovable, true);
    scene->addItem(pointItempp2);

    scene->addLine(QLineF(QPointF(generator->Path.firstpoint.p.x+centerpoint.x(), -generator->Path.firstpoint.p.y+centerpoint.y()),
                          QPointF(generator->Path.firstpoint.ppPoint.x+centerpoint.x(),-generator->Path.firstpoint.ppPoint.y+centerpoint.y())), QPen("yellow"));
    scene->addLine(QLineF(QPointF(generator->Path.secondpoint.ppPoint.x+centerpoint.x(), -generator->Path.secondpoint.ppPoint.y+centerpoint.y()),
                          QPointF(generator->Path.secondpoint.p.x+centerpoint.x(),-generator->Path.secondpoint.p.y+centerpoint.y())), QPen("yellow"));

}

/*
 * Zakładka derivative
 */
void MainWindow::on_MultiplierPppBox_valueChanged(double arg1)
{
    generator->MultiplierPpp = arg1;

    scene->clear();
    updateView();
}
void MainWindow::on_approximate_angleBox_stateChanged(int arg1)
{
    generator->approximate_angle = arg1;

    scene->clear();
    updateView();
}

void MainWindow::on_AP0pBox_valueChanged(double arg1)
{
    if(generator->control_deivative_edges)
    {
        if(!generator->control_curvature_on_the_edges)
        {
            generator->Path.firstpoint.thetaStopnie = arg1;
            generator->Path.firstpoint.DegToRad();
            generator->Path.firstpoint.ApproximateVector(360.0/generator->number_of_angles);
            if(update)
            {
                scene->clear();
                updateView();
            }
        }
    }
}
void MainWindow::on_LP0pBox_valueChanged(double arg1)
{
    if(generator->control_deivative_edges)
    {
        if(!generator->control_curvature_on_the_edges)
        {
            generator->Path.firstpoint.MoveAlongVector(arg1*generator->resolution/3);
            if(update)
            {
                scene->clear();
                updateView();
            }
        }
    }
}

void MainWindow::on_AP1pBox_valueChanged(double arg1)
{
    if(generator->control_deivative_edges)
    {
        if(!generator->control_curvature_on_the_edges)
        {
            generator->Path.secondpoint.thetaStopnie = arg1;
            generator->Path.secondpoint.DegToRad();
            generator->Path.secondpoint.MoveAlongVector(generator->Path.secondpoint.V);
            generator->Path.secondpoint.ApproximateVector(360.0/generator->number_of_angles);
            if(update)
            {
                scene->clear();
                updateView();
            }
        }
    }
}
void MainWindow::on_LP1pBox_valueChanged(double arg1)
{
    if(generator->control_deivative_edges)
    {
        if(!generator->control_curvature_on_the_edges)
        {
            generator->Path.secondpoint.MoveAlongVector(-arg1*generator->resolution/3);
            if(update)
            {
                scene->clear();
                updateView();
            }
        }
    }
}

void MainWindow::on_xP0ppBox_valueChanged(double arg1)
{
    generator->Path.firstpoint.ppPoint.x = arg1/6*generator->resolution+generator->Path.firstpoint.p.x;
    if(update)
    {
        scene->clear();
        updateView();
    }
}
void MainWindow::on_yP0ppBox_valueChanged(double arg1)
{
    generator->Path.firstpoint.ppPoint.y = arg1/6*generator->resolution+generator->Path.firstpoint.p.y;
    if(update)
    {
        scene->clear();
        updateView();
    }
}
void MainWindow::on_LP0ppBox_valueChanged(double arg1)
{
    double tmp = sqrt(pow((generator->Path.firstpoint.ppPoint.x-generator->Path.firstpoint.p.x),2) + pow((generator->Path.firstpoint.ppPoint.y-generator->Path.firstpoint.p.y),2));
    generator->Path.firstpoint.ppPoint.x = generator->Path.firstpoint.ppPoint.x*(arg1/6*generator->resolution/tmp)+generator->Path.firstpoint.p.x;
    generator->Path.firstpoint.ppPoint.y = generator->Path.firstpoint.ppPoint.y*(arg1/6*generator->resolution/tmp)+generator->Path.firstpoint.p.y;
    if(update)
    {
        scene->clear();
        updateView();
    }
}

void MainWindow::on_xP1ppBox_valueChanged(double arg1)
{
    generator->Path.secondpoint.ppPoint.x = arg1/6*generator->resolution+generator->Path.secondpoint.p.x;
    if(update)
    {
        scene->clear();
        updateView();
    }
}
void MainWindow::on_yP1ppBox_valueChanged(double arg1)
{
    generator->Path.secondpoint.ppPoint.y = arg1/6*generator->resolution+generator->Path.secondpoint.p.y;
    if(update)
    {
        scene->clear();
        updateView();
    }
}
void MainWindow::on_LP1ppBox_valueChanged(double arg1)
{
    double tmp = sqrt(pow(generator->Path.secondpoint.ppPoint.x-generator->Path.secondpoint.p.x,2)+pow(generator->Path.secondpoint.ppPoint.y-generator->Path.secondpoint.p.y,2));
    generator->Path.secondpoint.ppPoint.x = (generator->Path.secondpoint.ppPoint.x-generator->Path.secondpoint.p.x)*(arg1/6*generator->resolution/tmp)+generator->Path.secondpoint.p.x;
    generator->Path.secondpoint.ppPoint.y = (generator->Path.secondpoint.ppPoint.y-generator->Path.secondpoint.p.y)*(arg1/6*generator->resolution/tmp)+generator->Path.secondpoint.p.y;
    if(update)
    {
        scene->clear();
        updateView();
    }
}

void MainWindow::UpdatePppValues()
{
    ui->xP0ppBox->setValue(6*(generator->Path.firstpoint.ppPoint.x-generator->Path.firstpoint.p.x)/generator->resolution);
    ui->yP0ppBox->setValue(6*(generator->Path.firstpoint.ppPoint.y-generator->Path.firstpoint.p.y)/generator->resolution);
    ui->LP0ppBox->setValue(6*(sqrt(pow(generator->Path.firstpoint.ppPoint.x-generator->Path.firstpoint.p.x,2)+pow(generator->Path.firstpoint.ppPoint.y-generator->Path.firstpoint.p.y,2)))/generator->resolution);
    ui->xP1ppBox->setValue(6*(generator->Path.secondpoint.ppPoint.x-generator->Path.secondpoint.p.x)/generator->resolution);
    ui->yP1ppBox->setValue(6*(generator->Path.secondpoint.ppPoint.y-generator->Path.secondpoint.p.y)/generator->resolution);
    ui->LP1ppBox->setValue(6*(sqrt(pow(generator->Path.secondpoint.ppPoint.x-generator->Path.secondpoint.p.x,2)+pow(generator->Path.secondpoint.ppPoint.y-generator->Path.secondpoint.p.y,2)))/generator->resolution);
    generator->Path.firstpoint.UpdateValues();
    generator->Path.secondpoint.UpdateValues();
    ui->AP0pBox->setValue(generator->Path.firstpoint.thetaStopnie);
    ui->LP0pBox->setValue((generator->Path.firstpoint.V*3)/generator->resolution);
    ui->AP1pBox->setValue(generator->Path.secondpoint.thetaStopnie+180);
    ui->LP1pBox->setValue((generator->Path.secondpoint.V*3)/generator->resolution);
}

/*
 * Zakładka Nurbs
 */

void MainWindow::on_NurbsActive_Box_stateChanged(int arg1)
{
    displaySettings.NurbsActive = arg1;
    generator->NurbsActive = arg1;
    generator->nurbs.copy(generator->Path);

    scene->clear();
    updateView();
}
void MainWindow::on_NurbsActive2_Box_stateChanged(int arg1)
{
    displaySettings.Nurbs_control_deivative_edges = arg1;
    generator->Nurbs_control_deivative_edges = arg1;

    ui->NAP0pBox->setEnabled(arg1);
    ui->NLP0pBox->setEnabled(arg1);
    ui->NAP1pBox->setEnabled(arg1);
    ui->NLP1pBox->setEnabled(arg1);
    ui->NxP0ppBox->setEnabled(arg1);
    ui->NyP0ppBox->setEnabled(arg1);
    ui->NLP0ppBox->setEnabled(arg1);
    ui->NxP1ppBox->setEnabled(arg1);
    ui->NyP1ppBox->setEnabled(arg1);
    ui->NLP1ppBox->setEnabled(arg1);

    scene->clear();
    updateView();
}

void MainWindow::on_NxP0Box_valueChanged(double arg1)
{
    generator->nurbs.firstPoint.p.x = arg1*generator->resolution;
    if(update)
    {
        scene->clear();
        updateView();
    }
}
void MainWindow::on_NyP0Box_valueChanged(double arg1)
{
    generator->nurbs.firstPoint.p.y = arg1*generator->resolution;
    if(update)
    {
        scene->clear();
        updateView();
    }
}

void MainWindow::on_NxP1Box_valueChanged(double arg1)
{
    generator->nurbs.lastPoint.p.x = arg1*generator->resolution;
    if(update)
    {
        scene->clear();
        updateView();
    }
}
void MainWindow::on_NyP1Box_valueChanged(double arg1)
{
    generator->nurbs.lastPoint.p.y = arg1*generator->resolution;
    if(update)
    {
        scene->clear();
        updateView();
    }
}

void MainWindow::on_NAP0pBox_valueChanged(double arg1)
{
    if(generator->Nurbs_control_deivative_edges)
    {
        generator->nurbs.firstPoint.thetaStopnie = arg1;
        generator->nurbs.firstPoint.DegToRad();
        generator->nurbs.firstPoint.MoveAlongVector(generator->nurbs.firstPoint.V);
        generator->nurbs.firstPoint.ApproximateVector(360.0/generator->number_of_angles);
        if(update)
        {
            scene->clear();
            updateView();
        }
    }

}
void MainWindow::on_NLP0pBox_valueChanged(double arg1)
{
    if(generator->Nurbs_control_deivative_edges)
    {
        generator->nurbs.firstPoint.MoveAlongVector(arg1*generator->resolution/3);
        if(update)
        {
            scene->clear();
            updateView();
        }
    }

}

void MainWindow::on_NAP1pBox_valueChanged(double arg1)
{
    if(generator->Nurbs_control_deivative_edges)
    {
        generator->nurbs.lastPoint.thetaStopnie = arg1;
        generator->nurbs.lastPoint.DegToRad();
        generator->nurbs.lastPoint.MoveAlongVector(generator->nurbs.lastPoint.V);
        generator->nurbs.lastPoint.ApproximateVector(360.0/generator->number_of_angles);
        if(update)
        {
            scene->clear();
            updateView();
        }
    }
}
void MainWindow::on_NLP1pBox_valueChanged(double arg1)
{
    if(generator->Nurbs_control_deivative_edges)
    {
        generator->nurbs.lastPoint.MoveAlongVector(-arg1*generator->resolution/3);
        if(update)
        {
            scene->clear();
            updateView();
        }
    }
}

void MainWindow::on_NxP0ppBox_valueChanged(double arg1)
{
    if(generator->Nurbs_control_deivative_edges)
    {
        generator->nurbs.firstPoint.ppPoint.x = arg1/6*generator->resolution+generator->nurbs.firstPoint.p.x;
        if(update)
        {
            scene->clear();
            updateView();
        }
    }
}
void MainWindow::on_NyP0ppBox_valueChanged(double arg1)
{
    if(generator->Nurbs_control_deivative_edges)
    {
        generator->nurbs.firstPoint.ppPoint.y = arg1/6*generator->resolution+generator->nurbs.firstPoint.p.y;
        if(update)
        {
            scene->clear();
            updateView();
        }
    }
}
void MainWindow::on_NLP0ppBox_valueChanged(double arg1)
{
    if(generator->Nurbs_control_deivative_edges)
    {
        double tmp = sqrt(pow(generator->nurbs.firstPoint.ppPoint.x-generator->nurbs.firstPoint.p.x,2)+pow(generator->nurbs.firstPoint.ppPoint.y-generator->nurbs.firstPoint.p.y,2));
        generator->nurbs.firstPoint.ppPoint.x = (generator->nurbs.firstPoint.ppPoint.x-generator->nurbs.firstPoint.p.x)*((arg1/6)*generator->resolution/tmp)+generator->nurbs.firstPoint.p.x;
        generator->nurbs.firstPoint.ppPoint.y = (generator->nurbs.firstPoint.ppPoint.y-generator->nurbs.firstPoint.p.y)*((arg1/6)*generator->resolution/tmp)+generator->nurbs.firstPoint.p.y;
        if(update)
        {
            scene->clear();
            updateView();
        }
    }
}

void MainWindow::on_NxP1ppBox_valueChanged(double arg1)
{
    if(generator->Nurbs_control_deivative_edges)
    {
        generator->nurbs.lastPoint.ppPoint.x = arg1/6*generator->resolution+generator->nurbs.lastPoint.p.x;
        if(update)
        {
            scene->clear();
            updateView();
        }
    }
}
void MainWindow::on_NyP1ppBox_valueChanged(double arg1)
{
    if(generator->Nurbs_control_deivative_edges)
    {
        generator->nurbs.lastPoint.ppPoint.y = arg1*generator->resolution/6+generator->nurbs.lastPoint.p.y;
        if(update)
        {
            scene->clear();
            updateView();
        }
    }
}
void MainWindow::on_NLP1ppBox_valueChanged(double arg1)
{
    if(generator->Nurbs_control_deivative_edges)
    {
        generator->nurbs.lastPoint.MoveAlongVector(-arg1/6*generator->resolution/3);
        if(update)
        {
            scene->clear();
            updateView();
        }
    }
}

void MainWindow::UpdateNurbsValues()
{
    //Position
    ui->NxP0Box->setValue((generator->nurbs.firstPoint.p.x)/generator->resolution);
    ui->NyP0Box->setValue((generator->nurbs.firstPoint.p.y)/generator->resolution);

    ui->NxP1Box->setValue((generator->nurbs.lastPoint.p.x)/generator->resolution);
    ui->NyP1Box->setValue((generator->nurbs.lastPoint.p.y)/generator->resolution);

    generator->nurbs.firstPoint.UpdateValues();
    generator->nurbs.lastPoint.UpdateValues();
    //First derivative
    int ang= (generator->nurbs.firstPoint.thetaStopnie+360)*double(generator->number_of_angles)/360.0;
    ang = ang % 16;
    ui->NAP0pBox->setValue(double(ang) * 360.0/double(generator->number_of_angles));
    ui->NLP0pBox->setValue((generator->nurbs.firstPoint.V*3)/generator->resolution);

    ui->NAP1pBox->setValue(generator->nurbs.lastPoint.thetaStopnie+180.0);
    ui->NLP1pBox->setValue((generator->nurbs.lastPoint.V*3)/generator->resolution);

    //Second derivative
    ui->NxP0ppBox->setValue(6*(generator->nurbs.firstPoint.ppPoint.x-generator->nurbs.firstPoint.p.x)/generator->resolution);
    ui->NyP0ppBox->setValue(6*(generator->nurbs.firstPoint.ppPoint.y-generator->nurbs.firstPoint.p.y)/generator->resolution);
    ui->NLP0ppBox->setValue(6*(sqrt(pow(generator->nurbs.firstPoint.ppPoint.x-generator->nurbs.firstPoint.p.x,2)+pow(generator->nurbs.firstPoint.ppPoint.y-generator->nurbs.firstPoint.p.y,2)))/generator->resolution);

    ui->NxP1ppBox->setValue(6*(generator->nurbs.lastPoint.ppPoint.x-generator->nurbs.lastPoint.p.x)/generator->resolution);
    ui->NyP1ppBox->setValue(6*(generator->nurbs.lastPoint.ppPoint.y-generator->nurbs.lastPoint.p.y)/generator->resolution);
    //ui->NLP1ppBox->setValue((sqrt(pow(generator->nurbs.lastPoint.ppPoint.x-generator->nurbs.lastPoint.p.x,2)+pow(generator->nurbs.lastPoint.ppPoint.y-generator->nurbs.lastPoint.p.y,2)))/generator->resolution);
    ui->NLP1ppBox->setValue(6*(generator->nurbs.lastPoint.V*3)/generator->resolution);
}
