/**
 * \file bezierinterpolator.h
 * \brief Plik nagłówkowy klasy \a bezierinterpolator.
 *
 * Klasa \a bezierinterpolator przechowuje informację takie jak
 * jakość iterpolacji, wagę środkowych punktów krzywej Beziera.
 * Oraz przede wszystkim ma zaimplementowane takie funkcję jak wyznaczanie punktów interpolacyjnych krzywej Beziera
 * zarówno z offsetem jak i bez, czy wyznaczenie punktów pomocniczych algorytmem Boehma
 *
 * \see bezierinterpolator.cpp
 */

#ifndef BEZIERINTERPOLATOR_H
#define BEZIERINTERPOLATOR_H

#include <QPolygonF>
#include <QPointF>

#include <QtCore/qmath.h>
#include <QHash>
#include <iostream>
#include <cmath>
#include "Trajectory.h"
#include "Nurbs.h"

/**
 * @brief The BezierInterpolator class - przechowuje informację takie jak
 * jakość iterpolacji, wagę środkowych punktów krzywej Beziera.
 * Oraz przede wszystkim ma zaimplementowane takie funkcję jak wyznaczanie punktów interpolacyjnych krzywej Beziera
 * zarówno z offsetem jak i bez, czy wyznaczenie punktów pomocniczych algorytmem Boehma
 */
class BezierInterpolator {
public:
  BezierInterpolator();

  ///
  /// \brief Bezier - function that interpolates the points of the Bezier curve of the 3rd degree
  /// Interpolation is based on the location of 4 control points
  /// \param Trajectory - interpolated curve
  /// \param res - scaling factor of curvature
  ///
  void Bezier(Trajectory &Trajectory, double res);
  ///
  /// \brief Bezier - function that interpolates the points of the Bezier curve of the 3rd degree
  /// The interpolation is made based on the position of the 2 end points and
  /// the position of the point that marks the second derivative at the end points
  /// \param Trajectory - interpolated curve
  /// \param res - scaling factor of curvature
  /// \param M - scaling factor of the second derivative
  /// \param approximate_angle - make the angles approximate
  ///
  void Bezier(Trajectory &Trajectory, double res, double M, bool approximate_angle);

  ///
  /// \brief BezierNormal
  /// \param Trajectory
  ///
  void BezierNormal(Trajectory &Trajectory);
  void Bezier(Nurbs &nurbs,ControlPoint p1,ControlPoint p2,ControlPoint p3,ControlPoint p4, double res);

  /**
   * @brief BezierNormal - funkcja interpolująca punkty krzywej Beziera 3 stopnia
   * @param x1 - współrzędna x 1-go punktu krzywej Beziera
   * @param y1 - współrzędna y 1-go punktu krzywej Beziera
   * @param x2 - współrzędna x 2-go punktu krzywej Beziera
   * @param y2 - współrzędna y 2-go punktu krzywej Beziera
   * @param x3 - współrzędna x 3-go punktu krzywej Beziera
   * @param y3 - współrzędna y 3-go punktu krzywej Beziera
   * @param x4 - współrzędna x 4-go punktu krzywej Beziera
   * @param y4 - współrzędna y 4-go punktu krzywej Beziera
   * @param interpolatedPoints - element wyjściowy typu QPolygonF przechowujący informację
   * o zinterpolowanych punktach krzywej Beziera
   * @param w - waga 2-óch środkowych wierzchołków zadanego wieloboku
   */
  void BezierNormal(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4,
              QPolygonF &interpolatedPoints, QVector<double> &curvature, QPolygonF &curvaturePoints);


  /**
   * @brief SetQuality - ustawia wartość jakości interpolacji krzywej Beziera
   * Jakość interpolacji jest ustawiona prywatnie, więc dostęp jest jedynie przez tą funkcję
   * @param value - wartość przypisywana do parametru Quality
   */
  void SetMaxCurvature(double value);

  /**
   * @brief Quality - jakość interpolacji krzywej Beziera
   */
  double Quality;

  double LimitCurvature;

  bool poprawny_Trajectory;

private:

  double odleglosc(double x1, double y1, double x2, double y2);
};

#endif // BEZIERINTERPOLATOR_H
