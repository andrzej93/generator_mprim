/**
 * \file generator_motion_primitives.cpp
 * \brief Plik implementacji klasy \a generator_motion_primitives.
 *
 * Klasa \a generator_motion_primitives przechowuje informację takie jak
 * \see generator_motion_primitives.h
 */
#include "generator_motion_primitives.h"

generator_motion_primitives::generator_motion_primitives()
{


    resolution_m = 0.025000;
    number_of_angles = 16;
    total_number_of_primitives = 0;

    resolution = 50;

    for (int i = 0; i < number_of_angles; i++)
    {
        Trajectories_angle tmp;
        tmp.angleID = i;
        tmp.number_of_primitives = 0;
        trajectories_angle.push_back(tmp);
    }
    control_curvature_on_the_edges = 0;
}


/// interpolateCurve - Oblicza na nowo wszystkie Pointy interpolacji krzywej Beziera (+ Pointy pomocnicze)
void generator_motion_primitives::interpolateCurve()
{
    bezierInterpolator.SetMaxCurvature(Omegamax/Vmax);
    if(NurbsActive)
    {
        if(Nurbs_control_deivative_edges)
            nurbs.UpdatePoints();
        nurbs.interpolation_points.clear();
        nurbs.mprim_points.clear();
        //ControlPoint V0,V1,V2,V3;
        ControlPoint p1,p2,p3,p4,p5,p6;
        p3.SetValue((nurbs.fourthPoint.x-nurbs.thirdPoint.x)*(1.0/3.0) + nurbs.thirdPoint.x,(nurbs.fourthPoint.y-nurbs.thirdPoint.y)*(1.0/3.0) + nurbs.thirdPoint.y);
        p4.SetValue((nurbs.fourthPoint.x-nurbs.thirdPoint.x)*(2.0/3.0) + nurbs.thirdPoint.x,(nurbs.fourthPoint.y-nurbs.thirdPoint.y)*(2.0/3.0) + nurbs.thirdPoint.y);
        p1.SetValue((nurbs.thirdPoint.x-nurbs.firstPoint.vPoint.x)/2.0 + nurbs.firstPoint.vPoint.x,(nurbs.thirdPoint.y-nurbs.firstPoint.vPoint.y)/2.0 + nurbs.firstPoint.vPoint.y);
        p2.SetValue((p3.x-p1.x)/2.0 + p1.x,(p3.y-p1.y)/2.0 + p1.y);
        p6.SetValue((nurbs.fourthPoint.x-nurbs.lastPoint.vPoint.x)/2.0 + nurbs.lastPoint.vPoint.x,(nurbs.fourthPoint.y-nurbs.lastPoint.vPoint.y)/2.0 + nurbs.lastPoint.vPoint.y);
        p5.SetValue((p6.x-p4.x)/2.0 + p4.x,(p6.y-p4.y)/2.0 + p4.y);
        bezierInterpolator.Bezier(nurbs, nurbs.firstPoint.p, nurbs.firstPoint.vPoint, p1, p2, (resolution_m/resolution));
        bezierInterpolator.Bezier(nurbs, p2, p3, p4, p5, (resolution_m/resolution));
        bezierInterpolator.Bezier(nurbs, p5, p6, nurbs.lastPoint.vPoint, nurbs.lastPoint.p, (resolution_m/resolution));
        //bezierInterpolator.Bezier(nurbs, V0, V1, p1, p2, (resolution_m/resolution));
        //bezierInterpolator.Bezier(nurbs, p2, p3, p4, p5, (resolution_m/resolution));
        //bezierInterpolator.Bezier(nurbs, p5, p6, V2, V3, (resolution_m/resolution));
    }
    else
    {
        if(control_curvature_on_the_edges)
        {
            bezierInterpolator.Bezier(Path, (resolution_m/resolution), MultiplierPpp, approximate_angle);
        }
        else
        {
            bezierInterpolator.Bezier(Path, (resolution_m/resolution));
        }
    }

}


void generator_motion_primitives::SaveTrajectory()
{
    int tmp = int(round(Path.firstpoint.theta / ((360.0/number_of_angles) * M_PI / 180.0)));
    if(tmp>=0)
    {
        tmp = tmp % number_of_angles;
    }
    else
    {
        tmp = tmp + number_of_angles;
        tmp = tmp % number_of_angles;
    }
    Path.ID = trajectories_angle[tmp].number_of_primitives;
    trajectories_angle[tmp].trajektories.push_back(Path);

    trajectories_angle[tmp].number_of_primitives++;
    total_number_of_primitives++;
}
void generator_motion_primitives::ChangeNumberOfAngles()
{
    std::vector<Trajectories_angle> trajektorie_pom;
    trajektorie_pom = trajectories_angle;
    trajectories_angle.clear();

    for (int i = 0; i < number_of_angles; i++)
    {
        Trajectories_angle tmp;
        tmp.angleID = i;
        tmp.number_of_primitives = 0;
        trajectories_angle.push_back(tmp);
    }

    if((trajektorie_pom.size()==16)&&(number_of_angles==8))
    {
        for(int i=0; i<number_of_angles;i++)
        {
            trajectories_angle[i].trajektories= trajektorie_pom[2*i].trajektories;
            trajectories_angle[i].number_of_primitives = trajektorie_pom[2*i].number_of_primitives;
        }
    }
    else if((trajektorie_pom.size()==8)&&(number_of_angles==16))
    {
        for(int i=0; i<number_of_angles;i++)
        {
            trajectories_angle[2*i].trajektories= trajektorie_pom[i].trajektories;
            trajectories_angle[2*i].number_of_primitives = trajektorie_pom[i].number_of_primitives;
        }
    }
}
void generator_motion_primitives::SaveDataToFile()
{
    plik.open(path_file.c_str());


    plik << "resolution_m: " << std::fixed << std::setprecision( 6 ) << resolution_m << "\n";
    plik << "numberofangles: " << std::fixed << std::setprecision( 0 ) << number_of_angles << "\n";
    plik << "totalnumberofprimitives: " << total_number_of_primitives << "\n";


    for (int i = 0; i < number_of_angles; i++)
    {
        for (std::vector<Trajectory>::iterator TrajectoryIt = trajectories_angle[i].trajektories.begin(),
             TrajectoryEnd = trajectories_angle[i].trajektories.end(); TrajectoryIt != TrajectoryEnd; TrajectoryIt++)
        {
            plik << "primID: " << std::fixed << std::setprecision( 0 ) << TrajectoryIt->ID << "\n";
            /*if(TrajectoryIt->firstpoint.theta / ((360.0/number_of_angles) * M_PI / 180.0)>0)
                plik << "startangle_c: " << int(TrajectoryIt->firstpoint.theta / ((360.0/number_of_angles) * M_PI / 180.0))%16 << "\n";
            else
                plik << "startangle_c: " << int(TrajectoryIt->firstpoint.theta / ((360.0/number_of_angles) * M_PI / 180.0) + 16)%16 << "\n";
                */
            plik << "startangle_c: " << i << "\n";
            plik << "endpose_c: " << (TrajectoryIt->secondpoint.p.x)/resolution << " ";
            plik << (TrajectoryIt->secondpoint.p.y)/resolution << " ";
            if(int((TrajectoryIt->secondpoint.theta-M_PI) / ((360.0/number_of_angles) * M_PI / 180.0) + number_of_angles/2) > 0)
                plik << int(round((TrajectoryIt->secondpoint.theta-M_PI)/ ((360.0/number_of_angles) * M_PI / 180.0) + number_of_angles/2)) % number_of_angles << "\n";
            else
                plik << int(round((TrajectoryIt->secondpoint.theta-M_PI) / ((360.0/number_of_angles) * M_PI / 180.0) + number_of_angles/2 + number_of_angles)) % number_of_angles << "\n";
            plik << "additionalactioncostmult: " << TrajectoryIt->additionalactioncostmult << "\n";
            plik << "intermediateposes: " << TrajectoryIt->number_of_generat_mprim_points << "\n";

            for (std::vector<Point>::iterator pointIt = TrajectoryIt->mprim_points.begin(),
                 pointEnd = TrajectoryIt->mprim_points.end(); pointIt != pointEnd; pointIt++)
            {
                plik << std::fixed << std::setprecision( 4 );
                plik << (pointIt->x)/resolution*resolution_m << " ";
                plik << (pointIt->y)/resolution*resolution_m << " ";
                plik << (pointIt->theta) << "\n";
            }
        }
    }

    plik.close();
}

void generator_motion_primitives::TurnLeft()
{
    int tmp = round(Path.firstpoint.theta / ((360.0/number_of_angles) * M_PI / 180.0));
    Path.ID = trajectories_angle[tmp].number_of_primitives;

    Trajectory trajectory;
    trajectory.ID = Path.ID;
    trajectory.additionalactioncostmult = Path.additionalactioncostmult;

    for(int i = 0; i < Path.number_of_generat_mprim_points; i++)
    {
        Point point;
        point.u = i*(1.0/(Path.number_of_generat_mprim_points-1));

        point.x = 0;
        point.y = 0;
        point.curvature = 0;
        point.V_curvature.x = 0;
        point.V_curvature.y = 0;

        point.theta = Path.firstpoint.theta + point.u*(360.0/number_of_angles)*(M_PI/180);
        if(point.theta>=2*M_PI)
        {
            point.theta = point.theta-(2*M_PI);
        }
        trajectory.mprim_points.push_back(point);
        trajectory.interpolation_points.push_back(point);
    }
    trajectory.firstpoint.theta = Path.firstpoint.theta;
    trajectory.secondpoint.theta = trajectory.firstpoint.theta + ((360.0/number_of_angles) * M_PI / 180.0);// - ((360.0/2) * M_PI / 180.0);
    trajectory.copied=0;

    trajectories_angle[tmp].trajektories.push_back(trajectory);
    trajectories_angle[tmp].number_of_primitives++;
    total_number_of_primitives++;
}
void generator_motion_primitives::TurnRight()
{
    int tmp = round(Path.firstpoint.theta / ((360.0/number_of_angles) * M_PI / 180.0));
    Path.ID = trajectories_angle[tmp].number_of_primitives;

    Trajectory trajectory;
    trajectory.ID = Path.ID;
    trajectory.additionalactioncostmult = Path.additionalactioncostmult;

    for(int i = 0; i < Path.number_of_generat_mprim_points; i++)
    {
        Point point;
        point.u = i*(1.0/(Path.number_of_generat_mprim_points-1));

        point.x = 0;
        point.y = 0;
        point.curvature = 0;
        point.V_curvature.x = 0;
        point.V_curvature.y = 0;

        point.theta = Path.firstpoint.theta - point.u*(360.0/number_of_angles)*(M_PI/180);
        if(point.theta>2*M_PI)
        {
            point.theta = point.theta-(2*M_PI);
        }

        trajectory.mprim_points.push_back(point);
        trajectory.interpolation_points.push_back(point);
    }

    trajectory.firstpoint.theta = Path.firstpoint.theta;
    trajectory.secondpoint.theta = trajectory.firstpoint.theta - ((360.0/number_of_angles) * M_PI / 180.0);// - ((360.0/2) * M_PI / 180.0);
    trajectory.copied=0;

    trajectories_angle[tmp].trajektories.push_back(trajectory);
    trajectories_angle[tmp].number_of_primitives++;
    total_number_of_primitives++;
}

void generator_motion_primitives::Autocomplete()
{
    if(number_of_angles == 16)
    {
        for (std::vector<Trajectory>::iterator TrajectoryIt = trajectories_angle[0].trajektories.begin(),
             TrajectoryEnd = trajectories_angle[0].trajektories.end(); TrajectoryIt != TrajectoryEnd; TrajectoryIt++)
        {
            if(!TrajectoryIt->copied)
            {
                Przenies(TrajectoryIt,-1,1,1,4,0);
                Przenies(TrajectoryIt,-1,-1,0,8,0);
                Przenies(TrajectoryIt,1,-1,1,12,0);
                TrajectoryIt->copied = 1;
            }
        }
        for (std::vector<Trajectory>::iterator TrajectoryIt = trajectories_angle[1].trajektories.begin(),
             TrajectoryEnd = trajectories_angle[1].trajektories.end(); TrajectoryIt != TrajectoryEnd; TrajectoryIt++)
        {
            if(!TrajectoryIt->copied)
            {

                Przenies2(TrajectoryIt,1,1,1,3,1);
                Przenies(TrajectoryIt,-1,1,1,5,1);
                Przenies2(TrajectoryIt,-1,1,0,7,1);
                Przenies(TrajectoryIt,-1,-1,0,9,1);
                Przenies2(TrajectoryIt,-1,-1,1,11,1);
                Przenies(TrajectoryIt,1,-1,1,13,1);
                Przenies2(TrajectoryIt,1,-1,0,15,1);
                TrajectoryIt->copied = 1;
            }
        }
        for (std::vector<Trajectory>::iterator TrajectoryIt = trajectories_angle[2].trajektories.begin(),
             TrajectoryEnd = trajectories_angle[2].trajektories.end(); TrajectoryIt != TrajectoryEnd; TrajectoryIt++)
        {
            if(!TrajectoryIt->copied)
            {
                Przenies(TrajectoryIt,-1,1,1,6,2);
                Przenies(TrajectoryIt,-1,-1,0,10,2);
                Przenies(TrajectoryIt,1,-1,1,14,2);
                TrajectoryIt->copied = 1;
            }
        }
    }
    else if(number_of_angles == 8)
    {
        for (std::vector<Trajectory>::iterator TrajectoryIt = trajectories_angle[0].trajektories.begin(),
             TrajectoryEnd = trajectories_angle[0].trajektories.end(); TrajectoryIt != TrajectoryEnd; TrajectoryIt++)
        {
            if(!TrajectoryIt->copied)
            {
                Przenies(TrajectoryIt,-1,1,1,2,0);
                Przenies(TrajectoryIt,-1,-1,0,4,0);
                Przenies(TrajectoryIt,1,-1,1,6,0);
                TrajectoryIt->copied = 1;
            }
        }
        for (std::vector<Trajectory>::iterator TrajectoryIt = trajectories_angle[1].trajektories.begin(),
             TrajectoryEnd = trajectories_angle[1].trajektories.end(); TrajectoryIt != TrajectoryEnd; TrajectoryIt++)
        {
            if(!TrajectoryIt->copied)
            {
                Przenies(TrajectoryIt,-1,1,1,3,1);
                Przenies(TrajectoryIt,-1,-1,0,5,1);
                Przenies(TrajectoryIt,1,-1,1,7,1);
                TrajectoryIt->copied = 1;
            }
        }
    }
}

void generator_motion_primitives::Przenies(std::vector<Trajectory>::iterator przenoszone, double x, double y, bool swap, int angle, int angle_act)
{
    Trajectory tmp;
    tmp.additionalactioncostmult = przenoszone->additionalactioncostmult;
    tmp.ID = przenoszone->ID;
    tmp.number_of_generat_mprim_points = przenoszone->number_of_generat_mprim_points;
    tmp.number_of_interpolation_points = przenoszone->number_of_interpolation_points;

    if(swap)
    {
        tmp.firstpoint.SetValue(x*przenoszone->firstpoint.p.y, y*przenoszone->firstpoint.p.x,
                                    x*przenoszone->firstpoint.vPoint.y, y*przenoszone->firstpoint.vPoint.x);
        tmp.secondpoint.SetValue(x*przenoszone->secondpoint.p.y, y*przenoszone->secondpoint.p.x,
                                     x*przenoszone->secondpoint.vPoint.y, y*przenoszone->secondpoint.vPoint.x);
    }
    else
    {
        tmp.firstpoint.SetValue(x*przenoszone->firstpoint.p.x, y*przenoszone->firstpoint.p.y,
                                    x*przenoszone->firstpoint.vPoint.x, y*przenoszone->firstpoint.vPoint.y);
        tmp.secondpoint.SetValue(x*przenoszone->secondpoint.p.x, y*przenoszone->secondpoint.p.y,
                                     x*przenoszone->secondpoint.vPoint.x, y*przenoszone->secondpoint.vPoint.y);
    }
    tmp.firstpoint.theta = przenoszone->firstpoint.theta + double(angle-angle_act)*((360.0/number_of_angles) * M_PI / 180.0);
    tmp.secondpoint.theta = przenoszone->secondpoint.theta + double(angle-angle_act)*((360.0/number_of_angles) * M_PI / 180.0);
    /*
    if((przenoszone->secondpoint.p.x == 0)&&(przenoszone->secondpoint.p.y == 0))
    {
        tmp.firstpoint.theta = angle*((360.0/number_of_angles) * M_PI / 180.0);
        tmp.secondpoint.theta = (angle+1)*((360.0/number_of_angles) * M_PI / 180.0);
    }*/
    for (std::vector<Point>::iterator pointIt = przenoszone->interpolation_points.begin(),
         pointEnd = przenoszone->interpolation_points.end(); pointIt != pointEnd; pointIt++)
    {
        Point point;
        if(swap)
        {
            point.x = x*pointIt->y;
            point.y = y*pointIt->x;
        }
        else
        {
            point.x = x*pointIt->x;
            point.y = y*pointIt->y;
        }
        point.theta = pointIt->theta + (angle-angle_act)*((360.0/number_of_angles) * M_PI / 180.0);
        point.curvature = pointIt->curvature;
        tmp.interpolation_points.push_back(point);
    }
    for (std::vector<Point>::iterator pointIt = przenoszone->mprim_points.begin(),
         pointEnd = przenoszone->mprim_points.end(); pointIt != pointEnd; pointIt++)
    {
        Point point;
        if(swap)
        {
            point.x = x*pointIt->y;
            point.y = y*pointIt->x;
        }
        else
        {
            point.x = x*pointIt->x;
            point.y = y*pointIt->y;
        }
        point.theta = pointIt->theta + (angle-angle_act)*((360.0/number_of_angles) * M_PI / 180.0);
        point.curvature = pointIt->curvature;
        tmp.mprim_points.push_back(point);
    }
    trajectories_angle[angle].angleID = angle;
    trajectories_angle[angle].number_of_primitives++;
    total_number_of_primitives++;
    trajectories_angle[angle].trajektories.push_back(tmp);
}

void generator_motion_primitives::Przenies2(std::vector<Trajectory>::iterator przenoszone, double x, double y, bool swap, int angle, int angle_act)
{
    Trajectory tmp;
    tmp.additionalactioncostmult = przenoszone->additionalactioncostmult;
    tmp.ID = przenoszone->ID;
    tmp.number_of_generat_mprim_points = przenoszone->number_of_generat_mprim_points;
    tmp.number_of_interpolation_points = przenoszone->number_of_interpolation_points;

    if(swap)
    {
        tmp.firstpoint.SetValue(x*przenoszone->firstpoint.p.y, y*przenoszone->firstpoint.p.x,
                                    x*przenoszone->firstpoint.vPoint.y, y*przenoszone->firstpoint.vPoint.x);
        tmp.secondpoint.SetValue(x*przenoszone->secondpoint.p.y, y*przenoszone->secondpoint.p.x,
                                     x*przenoszone->secondpoint.vPoint.y, y*przenoszone->secondpoint.vPoint.x);
    }
    else
    {
        tmp.firstpoint.SetValue(x*przenoszone->firstpoint.p.x, y*przenoszone->firstpoint.p.y,
                                    x*przenoszone->firstpoint.vPoint.x, y*przenoszone->firstpoint.vPoint.y);
        tmp.secondpoint.SetValue(x*przenoszone->secondpoint.p.x, y*przenoszone->secondpoint.p.y,
                                     x*przenoszone->secondpoint.vPoint.x, y*przenoszone->secondpoint.vPoint.y);
    }
    tmp.firstpoint.theta = przenoszone->firstpoint.theta + double(angle-angle_act)*((360.0/number_of_angles) * M_PI / 180.0);
    tmp.secondpoint.theta = double(angle-angle_act+2)*((360.0/number_of_angles) * M_PI / 180.0) - przenoszone->secondpoint.theta;
    /*
    if((przenoszone->secondpoint.p.x == 0)&&(przenoszone->secondpoint.p.y == 0))
    {
        tmp.firstpoint.theta = angle*((360.0/number_of_angles) * M_PI / 180.0);
        tmp.secondpoint.theta = (angle+1)*((360.0/number_of_angles) * M_PI / 180.0);
    }*/
    for (std::vector<Point>::iterator pointIt = przenoszone->interpolation_points.begin(),
         pointEnd = przenoszone->interpolation_points.end(); pointIt != pointEnd; pointIt++)
    {
        Point point;
        if(swap)
        {
            point.x = x*pointIt->y;
            point.y = y*pointIt->x;
        }
        else
        {
            point.x = x*pointIt->x;
            point.y = y*pointIt->y;
        }
        point.theta = (angle-angle_act+2)*((360.0/number_of_angles) * M_PI / 180.0) - pointIt->theta;
        point.curvature = pointIt->curvature;
        tmp.interpolation_points.push_back(point);
    }
    for (std::vector<Point>::iterator pointIt = przenoszone->mprim_points.begin(),
         pointEnd = przenoszone->mprim_points.end(); pointIt != pointEnd; pointIt++)
    {
        Point point;
        if(swap)
        {
            point.x = x*pointIt->y;
            point.y = y*pointIt->x;
        }
        else
        {
            point.x = x*pointIt->x;
            point.y = y*pointIt->y;
        }
        point.theta = (angle-angle_act+2)*((360.0/number_of_angles) * M_PI / 180.0) - pointIt->theta;
        point.curvature = pointIt->curvature;
        tmp.mprim_points.push_back(point);
    }
    trajectories_angle[angle].angleID = angle;
    trajectories_angle[angle].number_of_primitives++;
    total_number_of_primitives++;
    trajectories_angle[angle].trajektories.push_back(tmp);
}
