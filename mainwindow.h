/**
 * \file bezierinterpolator.h
 * \brief Plik nagłówkowy klasy \a bezierinterpolator.
 *
 * Klasa \a bezierinterpolator przechowuje informację takie jak
 * jakość iterpolacji, wagę środkowych punktów krzywej Beziera.
 * Oraz przede wszystkim ma zaimplementowane takie funkcję jak wyznaczanie punktów interpolacyjnych krzywej Beziera
 * zarówno z offsetem jak i bez, czy wyznaczenie punktów pomocniczych algorytmem Boehma
 *
 * \see bezierinterpolator.cpp
 */
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QHash>
#include <QTreeWidgetItem>
#include "Trajectory.h"
#include "generator_motion_primitives.h"
#include <iostream>
#include <string>

class MovingEllipseItem;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    // Slots for ui
    void on_checkBox_1_stateChanged(int arg1);
    void on_checkBox_2_stateChanged(int arg1);
    void on_show_interpolated_pointscheckBox_stateChanged(int arg1);

    void itemChang(QTreeWidgetItem *item, int column);
    /**
     * @brief updateView - Uaktualnia widok krzywej na grafice graphicsView.
     * Uaktualnia widok na grafice graphicsView
     * Rysuje to co zostało oznaczone do narysowania.
     * @param skipPoint
     */
    void updateView();

    /**
     * @brief moveCurve - Przesuwa punkty kontrolne (wierzchołki)
     */
    void moveCurve();

    void makePlot();

    void on_SaveButton_clicked();
    void on_LeftButton_clicked();
    void on_RightButton_clicked();
    void on_SaveToFileButton_clicked();

    void on_Number_interpolate_point_spointSpinBox_valueChanged(double arg1);
    void on_Number_mprim_point_SpinBox_valueChanged(double arg1);
    void on_ResolutionSpinBox_valueChanged(double arg1);
    void on_L_angleSpinBox_valueChanged(double arg1);
    void on_CostSpinBox_valueChanged(double arg1);
    void on_VelocityMaxSpinBox_valueChanged(double arg1);
    void on_OmegaMaxSpinBox_valueChanged(double arg1);

    void on_AutocompleteButton_clicked();

    void on_All_curve_Box_stateChanged(int arg1);
    void on_Angle_curve_checkBox_stateChanged(int arg1);
    void on_ForgetButton_clicked();

    void on_BackMovecheckBox_stateChanged(int arg1);

    void on_control_curvature_edges_Box_stateChanged(int arg1);
    void on_MultiplierPppBox_valueChanged(double arg1);
    void on_approximate_angleBox_stateChanged(int arg1);

    void on_xP0ppBox_valueChanged(double arg1);
    void on_yP0ppBox_valueChanged(double arg1);
    void on_LP0ppBox_valueChanged(double arg1);
    void on_xP1ppBox_valueChanged(double arg1);
    void on_yP1ppBox_valueChanged(double arg1);
    void on_LP1ppBox_valueChanged(double arg1);

    void on_AP0pBox_valueChanged(double arg1);
    void on_LP0pBox_valueChanged(double arg1);
    void on_AP1pBox_valueChanged(double arg1);
    void on_LP1pBox_valueChanged(double arg1);
    void on_control_deivative_edges_Box_stateChanged(int arg1);

    void on_NurbsActive_Box_stateChanged(int arg1);

    void on_NxP0Box_valueChanged(double arg1);
    void on_NyP0Box_valueChanged(double arg1);
    void on_NxP1Box_valueChanged(double arg1);
    void on_NyP1Box_valueChanged(double arg1);
    void on_NAP0pBox_valueChanged(double arg1);
    void on_NLP0pBox_valueChanged(double arg1);
    void on_NAP1pBox_valueChanged(double arg1);
    void on_NLP1pBox_valueChanged(double arg1);
    void on_NxP0ppBox_valueChanged(double arg1);
    void on_NyP0ppBox_valueChanged(double arg1);
    void on_NLP0ppBox_valueChanged(double arg1);
    void on_NxP1ppBox_valueChanged(double arg1);
    void on_NyP1ppBox_valueChanged(double arg1);
    void on_NLP1ppBox_valueChanged(double arg1);

    void on_NurbsActive2_Box_stateChanged(int arg1);

private:
    Ui::MainWindow *ui;

    generator_motion_primitives *generator;

    friend class MovingEllipseItem;

    /**
     * @brief scene - Główna scena na której odbywa się wizualizacja
     */
    QGraphicsScene *scene;

    QPointF centerpoint;

    /**
     * @brief itemToPoint - Mapping of items on the scene to points in \var controlPoints.
     */
    QHash<MovingEllipseItem*, ControlPoint*> itemToPoint;

    void updateTree();

    void setCenterPoint();

    template <typename T>
    std::string to_string(T value);
    /**
     * @brief showRandomSpline - Generuje losowe punkty + uatkualnia widok (wraz z krzywą)
     * Usuwa poprzednio istniejące punkty, a na ich miejsce wstawia nowe o losowych współrzędnych
     * Punktów tych jest tyle samo ile przed naciśnięciem przycisku
     */
    void showRandomSpline();

    /// clearPoints
    /**
     * @brief clearPoints - Usuwa wszystkie punkty
     * Usuwa wszystkie punkty punkty kontolne wieloboku
     */
    void clearPoints();

    double ApproximatePosition(double var);

    void drawBezier();
    void drawNurbs();
    void drawExistingTrajectories();
    void drawPointsPP();

    void UpdatePppValues();
    void UpdateNurbsValues();

    bool update;

    /**
     * @brief The DisplaySettings struct - wyświetlacz opcji wyświetlania
     */
    struct DisplaySettings
    {
        DisplaySettings() : showInterpolatedPoints(false), showControlLines(true), showCurvature(false),
        showAllCurve(false), showAngleCurve(false), backmove(false), control_curvature_on_the_edges(false),
          control_deivative_edges(false), NurbsActive(false), Nurbs_control_deivative_edges(false){}

        /**
         * @brief showInterpolatedPoints
         * „Interp Point” - jeżeli jest zaznaczona ta opcja okno wyświetla zaznaczone punkty interpolowane (zaznaczone na czarno)
         */
        bool showInterpolatedPoints;
        /**
         * @brief showControlLines
         * „Control Line” - jeżeli jest zaznaczona to okno wyświetla linie wieloboku kontrolnego (początkowego) (zaznaczone na niebiesko),
         */
        bool showControlLines;
        /**
         * @brief showCurvature
         * „Add Line” - jeżeli jest zaznaczona to okno wyświetla linie wieloboku B-sklejanego (zaznaczone na czerwono),
         */
        bool showCurvature;

        bool showAllCurve;
        /**
         * @brief showCurvature
         * „Add Line” - jeżeli jest zaznaczona to okno wyświetla linie wieloboku B-sklejanego (zaznaczone na czerwono),
         */
        bool showAngleCurve;

        bool backmove;

        bool control_curvature_on_the_edges;
        bool control_deivative_edges;

        bool NurbsActive;
        bool Nurbs_control_deivative_edges;

    } displaySettings;
};

#endif // MAINWINDOW_H
